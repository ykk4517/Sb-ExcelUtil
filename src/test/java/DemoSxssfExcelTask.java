import com.ykkblog.excelutil.sxssf.AbstractSxssfExcelTask;
import com.ykkblog.excelutil.util.ExcelUtil;
import com.ykkblog.fastbase.exception.BusinessException;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;

/**
 * 渲染数据 SxssfExcelTask
 *
 * @author YKK
 * @date 2021/9/9 20:11
 **/

public class DemoSxssfExcelTask extends AbstractSxssfExcelTask {

    CellStyle style;

    /**
     * 初始化操作,尽情定义你的东西吧
     */
    @Override
    public void init() {
        style = ExcelUtil.getBorderStyle(BorderStyle.THIN, sheet);
    }

    /**
     * 渲染数据核心处理代码
     *
     * @param contentRow 当前行
     * @param rowIndex   当前行下标
     * @param rowType    当前行类型 0 标题；1 列名；2 内容
     */
    @Override
    public void handleRow(SXSSFRow contentRow, int rowIndex, Integer rowType) {
        // write your code
        if (startIndex + rowIndex == 2) {
            throw new BusinessException("出错了吧!");
        }
    }

    /**
     * 渲染数据核心处理代码
     *
     * @param contentCell 当前列
     * @param colIndex    当前列下标
     * @param obj         数据值
     * @param rowType     当前行类型 0 标题；1 列名；2 内容
     */
    @Override
    public void handleCell(SXSSFCell contentCell, int colIndex, Object obj, Integer rowType) {
        // write your code
        contentCell.setCellStyle(style);
    }

}