import com.ykkblog.excelutil.xlsx2html.XlsxToHtmlConverter;
import org.junit.Test;
import org.w3c.dom.Document;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * @author YKK
 * @date 2021/9/9 20:11
 **/

public class Xlsx2HtmlTest {

    @Test
    public void testXslx2Html() throws ParserConfigurationException, IOException, TransformerException {
        String [] args = new String[]{"\u202AC:\\Users\\YKK\\Desktop\\测试报表1638417595054.xlsx", "C:\\Users\\YKK\\Desktop\\test123.html"};
        args = new String[]{"E:\\Chrome Download\\工资信息_20220531174420.xlsx", "C:\\Users\\YKK\\Desktop\\test123.html"};
        //args = new String[]{"C:\\Users\\YKK\\Desktop\\测试报表1638499278819.xlsx", "C:\\Users\\YKK\\Desktop\\test123.html"};
        if (args.length < 2) {
            System.err.println("Usage: XlsxToHtmlConverter <inputFile.xlsx> <saveTo.html>");
            return;
        }

        System.out.println("Converting " + args[0]);
        System.out.println("Saving output to " + args[1]);

        XlsxToHtmlConverter xlsxToHtmlConverter = new XlsxToHtmlConverter();

        Document doc = xlsxToHtmlConverter.process(new File(args[0]));

        DOMSource domSource = new DOMSource(doc);
        StreamResult streamResult = new StreamResult(new File(args[1]));

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer serializer = tf.newTransformer();

        // 去掉Excel头行
        xlsxToHtmlConverter.setOutputColumnHeaders(false);
        // 去掉Excel行号
        xlsxToHtmlConverter.setOutputRowNumbers(false);

        // TODO set encoding from a command argument
        serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        serializer.setOutputProperty(OutputKeys.INDENT, "no");
        serializer.setOutputProperty(OutputKeys.METHOD, "html");
        serializer.transform(domSource, streamResult);
    }

    @Test
    public void testXslx2Html2() throws ParserConfigurationException, IOException, TransformerException {
        String [] args = new String[]{"\u202AC:\\Users\\YKK\\Desktop\\测试报表1638417595054.xlsx", "C:\\Users\\YKK\\Desktop\\test123.html"};
        // args = new String[]{"C:\\Users\\YKK\\Desktop\\测试报表1638499453814.xlsx", "C:\\Users\\YKK\\Desktop\\test123.html"};
        args = new String[]{"E:\\Chrome Download\\人事信息送审表_20220303180102.xlsx", "C:\\Users\\YKK\\Desktop\\test123.html"};
        if (args.length < 2) {
            System.err.println("Usage: XlsxToHtmlConverter <inputFile.xlsx> <saveTo.html>");
            return;
        }

        System.out.println("Converting " + args[0]);
        System.out.println("Saving output to " + args[1]);

        XlsxToHtmlConverter xlsxToHtmlConverter = new XlsxToHtmlConverter();

        Document doc = xlsxToHtmlConverter.process(new File(args[0]));

        DOMSource domSource = new DOMSource(doc);
        StreamResult streamResult = new StreamResult(new File(args[1]));

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer serializer = tf.newTransformer();

        // 去掉Excel头行
        xlsxToHtmlConverter.setOutputColumnHeaders(false);
        // 去掉Excel行号
        xlsxToHtmlConverter.setOutputRowNumbers(false);

        // TODO set encoding from a command argument
        serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        serializer.setOutputProperty(OutputKeys.INDENT, "no");
        serializer.setOutputProperty(OutputKeys.METHOD, "html");
        serializer.transform(domSource, streamResult);
    }

}
