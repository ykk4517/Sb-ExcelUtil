import com.ykkblog.excelutil.sxssf.SxssfExcelUtil;
import com.ykkblog.excelutil.util.ExcelExportColumn;
import com.ykkblog.excelutil.util.ExcelExportColumnCal;
import com.ykkblog.excelutil.util.ExcelExportConfig;
import com.ykkblog.excelutil.util.ExcelUtil;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.junit.Test;

import java.io.File;
import java.util.*;

/**
 * @author YKK
 * @date 2021/9/9 20:11
 **/

public class TestSxssf {
    public static final String[] TITLE = new String[]{"第1列", "第2列", "第3列", "第4列", "第5列", "第6列", "第7列"};
    public static final String SHEET_NAME = "page1";

    @Test
    public void test() {
        int num = 100;

        // 测试数据数量
        if (Objects.isNull(num)) {
            num = 65536;
        }
        List<List<Object>> content = buildContent(num);
        System.out.println("数据准备成功：" + num);

        List<ExcelExportColumn> columnList = new ArrayList<>();
        columnList.add(new ExcelExportColumnCal("分数"));
        columnList.add(new ExcelExportColumn("姓名"));
        columnList.add(new ExcelExportColumn("年龄"));
        columnList.add(new ExcelExportColumn("学历"));
        columnList.add(new ExcelExportColumn("地区"));
        columnList.add(new ExcelExportColumn("民族"));
        columnList.add(new ExcelExportColumn("籍贯"));
        columnList.add(new ExcelExportColumn("电话"));
        columnList.add(new ExcelExportColumn("性别"));

        long start = System.currentTimeMillis();
        ExcelExportConfig excelExportConfig = new ExcelExportConfig("广西自治区南宁市测试报表", "测试报表", columnList, content);
        excelExportConfig.setTitleSize(12);
        excelExportConfig.setColumnSize(10);
        excelExportConfig.setContentSize(10);

        try {
            SxssfExcelUtil sxssfExcelUtil = new SxssfExcelUtil(excelExportConfig);
            // 开始之前对SXSSFWorkbook做一些配置
            SXSSFWorkbook sxssfWorkbook = sxssfExcelUtil.getSxssfWorkbook();
            //SXSSFWorkbook wb = sxssfExcelUtil.getSxssfWorkbookByPageThread(DemoSxssfExcelTask.class);
            SXSSFWorkbook wb = sxssfExcelUtil.getSxssfWorkbookByPageThread(null);
            long millis = System.currentTimeMillis() - start;
            long second = millis / 1000;
            System.out.println("SXSSF Page Thread 导出" + num + "条数据，花费：" + second + "s/ " + millis + "ms");

            System.out.println("正在导出！");
            //excel文件名
            String fileName = excelExportConfig.getFileNamePrefix() + System.currentTimeMillis() + ".xlsx";

            ExcelUtil.exportByFile(wb, "C:\\Users\\YKK\\Desktop" + File.separator + fileName);
            wb.dispose();
        } catch (Exception e) {
            System.out.println("导出异常：");
            e.printStackTrace();
        }
    }

    /**
     * 构建内容
     *
     * @param num 模拟数据行数
     * @return 模拟数据
     */
    private List<List<Object>> buildContent(Integer num) {
        List<List<Object>> content = new ArrayList<>(num);
        for (int i = 0; i < num; i++) {
            int rowNum = i + 1;
            content.add(Arrays.asList(i + 1, rowNum + "0", Double.parseDouble(rowNum + "2"), 1236.32, new Date(), null, "", i + 1, rowNum + "0", Double.parseDouble(rowNum + "2"), 1236.32, new Date(), null, "", i + 1, rowNum + "0", Double.parseDouble(rowNum + "2"), 1236.32, new Date(), null, "", i + 1, rowNum + "0", Double.parseDouble(rowNum + "2"), 1236.32, new Date(), null, "", i + 1, rowNum + "0", Double.parseDouble(rowNum + "2"), 1236.32, new Date(), null, ""));
        }
        return content;
    }

}
