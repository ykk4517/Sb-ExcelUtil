package com.ykkblog.excelutil;

import com.ykkblog.excelutil.util.ExcelExportColumn;
import com.ykkblog.excelutil.util.ExcelExportColumnWrap;
import com.ykkblog.excelutil.util.ExcelExportConfig;
import com.ykkblog.excelutil.util.ExcelUtil;
import com.ykkblog.fastbase.util.DataUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.spel.standard.SpelExpression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.ykkblog.fastbase.util.ConstantBase.UNDERLINE;
import static com.ykkblog.fastbase.util.DateUtil.getStringDate;

/**
 * Excel工具类
 *
 * @author 姚康康
 * @date 2021/9/9 20:33
 */
@Slf4j
public class ExcelExportUtil {

    private final XSSFWorkbook xssfWorkbook;

    private final ExcelExportConfig excelExportConfig;

    /**
     * 标题样式（加粗，垂直居中）
     */
    XSSFCellStyle cellStyle;
    XSSFFont fontStyle;
    private final int contentSize = 12;

    /**
     * 开始时间
     */
    long startTimeMillis;


    public ExcelExportUtil(ExcelExportConfig excelExportConfig) {
        // 创建HSSFWorkbook对象
        this.xssfWorkbook = new XSSFWorkbook();
        this.excelExportConfig = excelExportConfig;
    }


    /**
     * 将数据写到Excel
     *
     * @return XSSFWorkbook
     * @throws Exception 异常
     */
    public XSSFWorkbook setListToExcel() throws Exception {
        startTimeMillis = System.currentTimeMillis();

        System.out.println("开始渲染：" + System.currentTimeMillis() + "ms");

        // 基础配置
        writeConfig(excelExportConfig);

        System.err.println("标题：" + excelExportConfig.getTitle());
        System.err.println("列数：" + excelExportConfig.getColumnList().size());
        System.out.println("行数：" + excelExportConfig.getData().size());

        // 创建HSSFSheet对象
        XSSFSheet hssfSheet = xssfWorkbook.createSheet(excelExportConfig.getFileNameFormat());
        cellStyle = xssfWorkbook.createCellStyle();

        // 标题样式
        XSSFCellStyle titleCellStyle = getCellStyle(xssfWorkbook.createCellStyle(), excelExportConfig.getTitleSize(), false, true);
        // 在第0行创建rows  (表标题)
        XSSFRow titleRow = hssfSheet.createRow(0);
        // 行高
        titleRow.setHeightInPoints(30);
        XSSFCell cellTitle = titleRow.createCell(0);
        cellTitle.setCellValue(excelExportConfig.getTitle());
        if (excelExportConfig.getColumnList().size() > 1) {
            // 合并单元格
            // 给定要合并的单元格范围
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, excelExportConfig.getColumnList().size() - 1);
            // 这是合并单元格过程
            hssfSheet.addMergedRegion(region);
            // 给合并过的单元格加边框
            ExcelUtil.setBorderStyle(BorderStyle.THIN, region, hssfSheet);
        }
        cellTitle.setCellStyle(titleCellStyle);


        // 列头样式
        XSSFCellStyle headCellStyle = getCellStyle(xssfWorkbook.createCellStyle(), excelExportConfig.getColumnList().get(0).getColumnSize(), false, true);

        // 在第1行创建rows
        XSSFRow row = hssfSheet.createRow(1);
        // 设置列头元素
        XSSFCell cellHead;
        String hName;
        for (int columnIndex = 0; columnIndex < excelExportConfig.getColumnList().size(); columnIndex++) {
            cellHead = row.createCell(columnIndex);
            hName = excelExportConfig.getColumnList().get(columnIndex).getName();
            if (hName != null) {
                cellHead.setCellValue(hName);
            } else {
                cellHead.setCellValue("");
            }
            cellHead.setCellStyle(getCellStyle(headCellStyle, excelExportConfig.getColumnList().get(columnIndex).getColumnSize()));

        }

        // 内容样式
        XSSFCellStyle bodyCellStyle = getCellStyle(xssfWorkbook.createCellStyle(), excelExportConfig.getColumnList().get(0).getContentSize(), false, false);

        System.out.println("开始渲染：" + (System.currentTimeMillis() - startTimeMillis) + "ms");
        // 写入中间样式、数据
        writeContent(excelExportConfig, hssfSheet, bodyCellStyle);

        System.out.println("渲染用时：" + (System.currentTimeMillis() - startTimeMillis) + "ms");
        // 进行合计、调整列宽
        // dealSum(excelExportConfig, hssfSheet);
        System.out.println("合计用时：" + (System.currentTimeMillis() - startTimeMillis) + "ms");

        return xssfWorkbook;
    }


    /**
     * 基于Response返回Excel
     *
     * @param response 返回流
     * @throws IOException 文件流异常
     */
    public void exportExcelByResponse(HttpServletResponse response) throws IOException {
        // //TODO 下载
        String fileName = excelExportConfig.getFileNamePrefix() +
                (excelExportConfig.getFileNameFormat() == null ? "" : UNDERLINE + getStringDate(excelExportConfig.getFileNameFormat()));
        ExcelUtil.exportByResponse(response, xssfWorkbook, fileName);
    }

    /**
     * 导出到文件
     *
     * @param path 路径
     * @return 导出是否成功 返回文件名
     * @throws Exception 异常
     */
    public String exportExcelByFile(String path) throws Exception {
        // 保存到文件
        final String underline = "_";
        String fileName = excelExportConfig.getFileNamePrefix() + (excelExportConfig.getFileNameFormat() == null ? "" :
                underline + getStringDate(excelExportConfig.getFileNameFormat())) + ".xlsx";

        ExcelUtil.exportByFile(xssfWorkbook, path + File.separator + fileName);
        return fileName;
    }

    /**
     * 导出基础配置
     *
     * @param excelExportConfig Excel配置
     * @throws Exception 异常
     */
    private void writeConfig(ExcelExportConfig excelExportConfig) throws Exception {

        // 检查参数配置信息
        if (excelExportConfig.getFileNameFormat() == null || "".equals(excelExportConfig.getFileNameFormat())) {
            String title = "报表";
            excelExportConfig.setFileNameFormat(title);
            // throw new Exception("表名不能为NULL");
        }

        if (excelExportConfig.getColumnList() == null || excelExportConfig.getColumnList().size() == 0) {
            throw new Exception("列名数组不能为空或者为NULL");
        }

        if (excelExportConfig.getContentSize() < 0) {
            excelExportConfig.setContentSize(contentSize);
            // throw new Exception("字体、宽度或者高度不能为负值");
        }

        if (excelExportConfig.getFileNamePrefix() == null || "".equals(excelExportConfig.getFileNamePrefix())) {
            String sheetName = "sheet";
            excelExportConfig.setFileNamePrefix(sheetName);
            // throw new Exception("工作表表名不能为NULL");
        }

        if (excelExportConfig.isRowNum()) {
            excelExportConfig.getColumnList().add(0, new ExcelExportColumn("序号"));
        }
    }

    /**
     * 写入中间样式、数据
     *
     * @param excelExportConfig Excel配置
     * @param hssfSheet         sheet对象
     * @param bodyCellStyle     单元格样式
     */
    private void writeContent(ExcelExportConfig excelExportConfig, Sheet hssfSheet, XSSFCellStyle bodyCellStyle) {
        List<Object> listData;
        Row rowBody;
        Cell cellBody;
        Object valueObj;
        String value;
        for (int di = 0; di < excelExportConfig.getData().size(); di++) {
            listData = excelExportConfig.getData().get(di);

            rowBody = hssfSheet.createRow(di + 2);
            int ma = excelExportConfig.isRowNum() ? listData.size() + 1 : listData.size();
            for (int ci = 0; ci < ma; ci++) {
                int index = ci == 0 ? 0 : excelExportConfig.isRowNum() ? ci - 1 : ci;
                cellBody = rowBody.createCell(ci);
                if (excelExportConfig.isRowNum() && ci == 0) {
                    cellBody.setCellValue(di + 1);
                } else {
                    if (excelExportConfig.isRowNum()) {
                        valueObj = listData.get(ci - 1);
                    } else {
                        valueObj = listData.get(ci);
                    }

                    value = String.valueOf(valueObj);
                    cellBody.setCellType(CellType.STRING);
                    cellBody.setCellValue(DataUtil.isNotEmpty(value) ? value : "");
                }
                // 设置单个单元格的字体颜色
                // cellBody.setCellStyle(getCellStyle(bodyCellStyle, excelExportConfig.getColumnList().get(index).getContentSize()));
            }
        }

    }

    /**
     * 进行合计、调整列宽
     *
     * @param excelExportConfig Excel配置
     * @param sheet             sheet对象
     */
    private void dealSum(ExcelExportConfig excelExportConfig, Sheet sheet) {

        int lastRow = excelExportConfig.getData().size() + 2;
        Row rowCount = sheet.createRow(lastRow);
        boolean availableCount = true;
        // 内容样式
        XSSFCellStyle calFirstCellStyle = getCellStyle(xssfWorkbook.createCellStyle(), contentSize, false, true);
        XSSFCellStyle calItemCellStyle = getCellStyle(xssfWorkbook.createCellStyle(), contentSize, true, true);
        // 设置最大宽度
        for (int wi = 0; wi < excelExportConfig.getColumnList().size(); wi++) {

            // 是否有合计行
            if (availableCount) {
                List<ExcelExportColumn> collect = excelExportConfig.getColumnList().stream().filter(ExcelExportColumn::getCal).collect(Collectors.toList());

                if (collect.size() == 0) {
                    availableCount = false;
                    continue;
                }

                Cell hssfCell = rowCount.createCell(0);
                hssfCell.setCellValue("合计");
                hssfCell.setCellStyle(calFirstCellStyle);
                // 求和操作
                Cell cellCount = rowCount.createCell(wi);
                cellCount.setCellStyle(calItemCellStyle);

                if (excelExportConfig.getColumnList().get(wi).getCal()) {
                    // 长度转成ABC列
                    String colString = CellReference.convertNumToColString(wi);
                    // System.out.println(colString);
                    // 求和公式 求i9至i12单元格的总和
                    String sumString = "SUM(" + colString + "3:" + colString + lastRow + ")";
                    // System.out.println(sumString);
                    // 把公式塞入合计列
                    cellCount.setCellFormula(sumString);
                }
            }

            // sheet.autoSizeColumn(wi);
            // ExcelBaseUtil.setSizeColumn(sheet, wi, 1, lastRow);

        }

    }


    /**
     * 调整单元格样式基础配置
     *
     * @param cellStyle 单元格样式
     * @param textSize  文字大小
     * @return XSSFCellStyle
     */
    private XSSFCellStyle getCellStyle(XSSFCellStyle cellStyle, int textSize) {
        //设置字体
        // 设置标题字体大小
        cellStyle.getFont().setFontHeightInPoints((short) textSize);
        return cellStyle;
    }

    /**
     * 单元格样式基础配置
     *
     * @param cellStyle 单元格样式
     * @param textSize  文字大小
     * @param bold      是否加粗
     * @param bg        背景颜色
     * @return XSSFCellStyle
     */
    private XSSFCellStyle getCellStyle(XSSFCellStyle cellStyle, int textSize, boolean bold, boolean bg) {

        // 水平居中
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        // 垂直居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        // 设置边框

        if (bg) {
            // 背景颜色
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            cellStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(214, 220, 228)));
        }
        // 边框
        BorderStyle borderStyle = BorderStyle.THIN;
        cellStyle.setBorderBottom(borderStyle);
        cellStyle.setBorderLeft(borderStyle);
        cellStyle.setBorderRight(borderStyle);
        cellStyle.setBorderTop(borderStyle);
        cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());


        // 设置字体
        fontStyle = xssfWorkbook.createFont();
        // 加粗
        fontStyle.setBold(bold);
        // 设置标题字体大小
        fontStyle.setFontHeightInPoints((short) textSize);
        cellStyle.setFont(fontStyle);
        return cellStyle;
    }

    /**
     * 导出Excel报表
     *
     * @param beans   存放数据对象的容器，每个元素将独立生成一行
     * @param columns 列元数据，请告诉我报表具有哪些列。
     *                key是个SPEL字符串，表示一个列的数据来源（property path），该SPEL的root对象将指向beans的各个元素；
     *                value可指定当前列的元数据，包括列标题、字体大小等。
     * @return 返回是否导出成功。
     */
    public static List<List<Object>> getColulmnList(Collection<?> beans, Map<String, ExcelExportColumn> columns) {
        // SPEL解释器
        SpelExpressionParser spelExpressionParser = new SpelExpressionParser();
        // SPEL上下文
        EvaluationContext evaluationContext = SimpleEvaluationContext.forReadOnlyDataBinding().build();
        // SPEL
        Map<String, SpelExpression> els = new HashMap<>(columns.size());
        for (String field : columns.keySet()) {
            els.put(field, spelExpressionParser.parseRaw(field));
        }

        // 转换表格数据 List<Bean> -> List<List<?>>
        // 行
        return beans.stream()
                // 单元格
                .map(replyDetail -> columns.keySet().stream()
                        .map(field -> {
                            try {
                                return els.get(field).getValue(evaluationContext, replyDetail);
                            } catch (Exception e) {
                                log.error(field, e);
                                e.printStackTrace();
                                return "错误";
                            }
                        }).collect(Collectors.toList())
                ).collect(Collectors.toList());
    }


    public static void main(String[] args) {
//        readTest();
//        writeTest();



    }


    public static void readTest() {
        System.out.println("readTest");
    }


    public static void writeTest() {

    }

    private static void spelTest() {
        List<ExcelExportConfig> dataList = new ArrayList<>();

        List<ExcelExportColumn> columnList = new ArrayList();
        columnList.add(new ExcelExportColumn("1.1"));
        columnList.add(new ExcelExportColumn("1.2"));

        List<Map<String, Object>> mapData = new ArrayList<>();
        Map<String, Object> mm = new HashMap<>();
        mm.put("name", "1.1mm");
        mm.put("age", "1.2age");
        mapData.add(mm);
        mm.put("name", "1.2mm");
        mm.put("age", "1.2age");
        mapData.add(mm);

        dataList.add(new ExcelExportConfig(columnList, mapData, "一", "1"));

        columnList = new ArrayList();
        columnList.add(new ExcelExportColumn("2.1"));
        columnList.add(new ExcelExportColumn("2.2"));

        mapData = new ArrayList<>();
        mm = new HashMap<>();
        mm.put("name", "2.1mm");
        mm.put("age", "2.1age");
        mapData.add(mm);
        mm.put("name", "2.2mm");
        mm.put("age", "2.2age");
        mapData.add(mm);

        dataList.add(new ExcelExportConfig(columnList, mapData, "二", "2"));

        columnList = new ArrayList();
        columnList.add(new ExcelExportColumn("3.1"));
        columnList.add(new ExcelExportColumn("3.2"));

        mapData = new ArrayList<>();
        mm = new HashMap<>();
        mm.put("name", "3.1mm");
        mm.put("age", "3.1age");
        mapData.add(mm);
        mm = new HashMap<>();
        mm.put("name", "3.2mm");
        mm.put("age", "3.2age");
        mapData.add(mm);

        dataList.add(new ExcelExportConfig(columnList, mapData, "三", "3"));

//        System.out.println(dataList);

        Map<String, ExcelExportColumn> columns = new LinkedHashMap<>();
        columns.put("fileNamePrefix", new ExcelExportColumn("送审编号", 64));
//        columns.put("columnList[0].name", new ExcelExportColumn("送审编号.1", 64));
//        columns.put("columnList[1].name", new ExcelExportColumn("送审编号.2", 64));
        columns.put("mapData[0]", new ExcelExportColumn("mapList", 64));
        columns.put("mapData[0]['name']", new ExcelExportColumn("mapList", 64));
        columns.put("mapData[0]['age']", new ExcelExportColumn("mapList", 64));

//        System.out.println(columns);

        List<List<Object>> colulmnList = getColulmnList(dataList, columns);
        System.out.println(colulmnList);
    }

}
