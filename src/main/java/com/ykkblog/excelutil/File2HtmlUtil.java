package com.ykkblog.excelutil;

import com.ykkblog.excelutil.xlsx2html.XlsxToHtmlConverter;
import org.apache.poi.hssf.converter.ExcelToHtmlConverter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Excel转Html工具类
 *
 * @author 姚康康
 * @date 2021/9/9 20:11
 */
public class File2HtmlUtil {

    /**
     * excel03 xls 转html 文件
     *
     * @param filePath 文件路径
     * @param response res
     **/
    public static void xlsToHtml(String filePath, HttpServletResponse response) throws Exception {
        InputStream inputStream = new FileInputStream(filePath);
        xlsToHtml(inputStream, response);
    }

    /**
     * excel03 xls 转html 输入流
     *
     * @param inputStream inputStream
     * @param response    res
     **/
    public static void xlsToHtml(InputStream inputStream, HttpServletResponse response) throws Exception {
        HSSFWorkbook excelBook = new HSSFWorkbook(inputStream);
        xlsToHtml(excelBook, response);
    }

    /**
     * 区别在于HSSFWorkbook主要读取的是.xls格式的文件，
     *
     * @param hssfWorkbook hssfWorkbook
     * @param response     response
     * @throws Exception 业务异常
     */
    public static void xlsToHtml(HSSFWorkbook hssfWorkbook, HttpServletResponse response) throws Exception {

        // 删掉其他sheet
        for (int i = 1; i < hssfWorkbook.getNumberOfSheets(); i++) {
            hssfWorkbook.removeSheetAt(i);
        }

        ExcelToHtmlConverter excelToHtmlConverter = new ExcelToHtmlConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());

        // https://blog.csdn.net/qq_35624642/article/details/79239755
        // 去掉Excel头行
        excelToHtmlConverter.setOutputColumnHeaders(false);
        // 去掉Excel行号
        excelToHtmlConverter.setOutputRowNumbers(false);

        // excel转html
        excelToHtmlConverter.processWorkbook(hssfWorkbook);
        transform(response, excelToHtmlConverter.getDocument());
    }

    /**
     * excel09 xlsx 转html 文件
     *
     * @param filePath 文件路径
     * @param response res
     **/
    public static void xlsxToHtml(String filePath, HttpServletResponse response) throws Exception {
        InputStream inputStream = new FileInputStream(filePath);
        xlsxToHtml(inputStream, response);
    }

    /**
     * excel09 xlsx 转html 输入流
     *
     * @param inputStream inputStream
     * @param response    res
     **/
    public static void xlsxToHtml(InputStream inputStream, HttpServletResponse response) throws Exception {
        XSSFWorkbook excelBook = new XSSFWorkbook(inputStream);
        xlsxToHtml(excelBook, response);
    }

    /**
     * excel09 xlsx 转html 输入流
     *
     * @param inputStream inputStream
     * @param response    res
     **/
    public static void xlsxToHtml(InputStream inputStream, HttpServletResponse
            response, Integer renderStartIndex, Integer renderEndIndex) throws Exception {
        XSSFWorkbook excelBook = new XSSFWorkbook(inputStream);
        xlsxToHtml(excelBook, response, renderStartIndex, renderEndIndex);
    }

    /**
     * 区别在于XSSFWorkbook主要读取的是.xlsx格式的文件，
     *
     * @param xssfWorkbook xssfWorkbook
     * @param response     response
     * @throws Exception 业务异常
     */
    public static void xlsxToHtml(XSSFWorkbook xssfWorkbook, HttpServletResponse response) throws Exception {
        XlsxToHtmlConverter xlsxToHtmlConverter = new XlsxToHtmlConverter();
        // excel转html
        xlsxToHtmlConverter.processWorkbook(xssfWorkbook);
        // 返回流
        transform(response, xlsxToHtmlConverter.getDocument());
    }

    /**
     * 区别在于XSSFWorkbook主要读取的是.xlsx格式的文件，
     *
     * @param xssfWorkbook     xssfWorkbook
     * @param response         response
     * @param renderStartIndex renderStartIndex
     * @param renderEndIndex   renderEndIndex
     * @throws Exception 业务异常
     */
    public static void xlsxToHtml(XSSFWorkbook xssfWorkbook, HttpServletResponse
            response, Integer renderStartIndex, Integer renderEndIndex) throws Exception {
        XlsxToHtmlConverter xlsxToHtmlConverter = new XlsxToHtmlConverter();
        // excel转html
        xlsxToHtmlConverter.processWorkbook(xssfWorkbook, renderStartIndex, renderEndIndex);
        // 返回流
        transform(response, xlsxToHtmlConverter.getDocument());
    }

    /**
     * 转换
     *
     * @param response     返回流
     * @param htmlDocument htmlDocument
     * @throws Exception 异常
     */
    private static void transform(HttpServletResponse response, Document htmlDocument) throws Exception {
        // 字节数组输出流
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        DOMSource domSource = new DOMSource(htmlDocument);
        StreamResult streamResult = new StreamResult(outStream);
        /* 将document中的内容写入文件中，创建html页面 */
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer serializer = tf.newTransformer();
        serializer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
        serializer.setOutputProperty(OutputKeys.INDENT, "yes");
        serializer.setOutputProperty(OutputKeys.METHOD, "html");
        serializer.transform(domSource, streamResult);
        outStream.close();
        OutputStream outputStream = response.getOutputStream();
        // 这里填表单访问接口
        outputStream.write(outStream.toByteArray());
        outputStream.flush();
        outputStream.close();
    }
}
