package com.ykkblog.excelutil.sxssf;

import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;

/**
 * 渲染数据 默认SxssfExcelTask
 *
 * @author YKK
 * @date 2021/9/9 20:11
 **/

public class DefaultSxssfSxssfExcelTask extends AbstractSxssfExcelTask {

    /**
     * 初始化操作,尽情定义你的东西吧
     */
    @Override
    public void init() {
        // 冻结列行
        sheet.createFreezePane(2, 2, 2, 2);
    }

    /**
     * 渲染数据核心处理代码
     *
     * @param contentRow 当前行
     * @param rowIndex   当前行下标
     * @param rowType    当前行类型 0 标题；1 列名；2 内容
     */
    @Override
    public void handleRow(SXSSFRow contentRow, int rowIndex, Integer rowType) {
        // write your code
    }

    /**
     * 渲染数据核心处理代码
     *
     * @param contentCell 当前列
     * @param colIndex    当前列下标
     * @param obj         数据值
     * @param rowType     当前行类型 0 标题；1 列名；2 内容
     */
    @Override
    public void handleCell(SXSSFCell contentCell, int colIndex, Object obj, Integer rowType) {
        // write your code
    }

}