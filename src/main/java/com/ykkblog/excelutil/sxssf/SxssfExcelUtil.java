package com.ykkblog.excelutil.sxssf;

import com.ykkblog.excelutil.util.ExcelExportColumn;
import com.ykkblog.excelutil.util.ExcelExportConfig;
import com.ykkblog.excelutil.util.ExcelUtil;
import com.ykkblog.fastbase.util.DataUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.ykkblog.fastbase.util.ConstantBase.UNDERLINE;
import static com.ykkblog.fastbase.util.DateUtil.getStringDate;

/**
 * ————————————————
 * 版权声明：本文为CSDN博主「非常王不二」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/u012712257/article/details/88355167
 *
 * @author YKK
 * @date 2021/9/9 20:11
 **/
@Data
@Slf4j
public class SxssfExcelUtil {
    public int perTaskLimit = 90000;

    /**
     * SXSSFWorkbook对象
     */
    private SXSSFWorkbook sxssfWorkbook;
    /**
     * XSSFWorkbook对象
     */
    // private XSSFWorkbook xssfWorkbook;

    /**
     * Excel配置
     */
    private final ExcelExportConfig excelExportConfig;

    public SxssfExcelUtil(ExcelExportConfig excelExportConfig) {
        // 创建SXSSFWorkbook对象
        // this.xssfWorkbook = new XSSFWorkbook();
        this.sxssfWorkbook = new SXSSFWorkbook();
        this.excelExportConfig = excelExportConfig;
    }

    public SxssfExcelUtil(SXSSFWorkbook sxssfWorkbook, ExcelExportConfig excelExportConfig) {
        // 创建SXSSFWorkbook对象
        // this.xssfWorkbook = new XSSFWorkbook();
        this.sxssfWorkbook = sxssfWorkbook;
        this.excelExportConfig = excelExportConfig;
    }

    public SxssfExcelUtil(ExcelExportConfig excelExportConfig, Integer perTaskLimit) {
        // 创建SXSSFWorkbook对象
        this.sxssfWorkbook = new SXSSFWorkbook();
        this.excelExportConfig = excelExportConfig;
        this.perTaskLimit = perTaskLimit;
    }

    public SxssfExcelUtil(ExcelExportConfig excelExportConfig, Boolean noFlush) {
        // 创建SXSSFWorkbook对象
        this.sxssfWorkbook = new SXSSFWorkbook(noFlush ? -1 : SXSSFWorkbook.DEFAULT_WINDOW_SIZE);
        this.excelExportConfig = excelExportConfig;
    }

    public SXSSFWorkbook getSxssfWorkbookByPageThread(Class<? extends AbstractSxssfExcelTask> cls) throws Exception {

        // 导出前检查
        writeConfig();

        AbstractSxssfExcelTask sxssfExcelTask;

        boolean defaultMode = excelExportConfig.getMapData() == null || excelExportConfig.getMapData().size() == 0;
        int dataSize = defaultMode ? excelExportConfig.getData() != null ? excelExportConfig.getData().size() : 0 : excelExportConfig.getMapData().size();

        log.error("总条数：" + dataSize);
        int pageNum = dataSize / perTaskLimit;
        int lastCount = dataSize % perTaskLimit;
        if (dataSize <= perTaskLimit) {
            SXSSFSheet sheet = sxssfWorkbook.createSheet(excelExportConfig.getTitle());
            if (defaultMode) {
                sxssfExcelTask = createExcelTask(cls, 0, sheet, excelExportConfig.getData(), null);
            } else {
                sxssfExcelTask = createExcelTask(cls, 0, sheet, null, excelExportConfig.getMapData());
            }
            sxssfExcelTask.call();
            return sxssfWorkbook;
        }

        int partIndex = lastCount == 0 ? pageNum : pageNum + 1;
        ExecutorService executorService = Executors.newFixedThreadPool(partIndex);
        Set<Future<Void>> futureSet = new HashSet<>();
        try {
            for (int c = 1; c <= pageNum; c++) {
                SXSSFSheet sheet = sxssfWorkbook.createSheet(excelExportConfig.getTitle() + c);
                int startIndex = (c - 1) * perTaskLimit;
                if (defaultMode) {
                    sxssfExcelTask = createExcelTask(cls, startIndex, sheet, excelExportConfig.getData().subList(startIndex, c * perTaskLimit), null);
                } else {
                    sxssfExcelTask = createExcelTask(cls, startIndex, sheet, null, excelExportConfig.getMapData().subList(startIndex, c * perTaskLimit));
                }
                futureSet.add(executorService.submit(sxssfExcelTask));
            }
            if (lastCount != 0) {
                SXSSFSheet sheet = sxssfWorkbook.createSheet(excelExportConfig.getTitle() + (pageNum + 1));
                int startIndex = pageNum * perTaskLimit;
                if (defaultMode) {
                    sxssfExcelTask = createExcelTask(cls, startIndex, sheet,
                            excelExportConfig.getData().subList(startIndex, pageNum * perTaskLimit + lastCount), null);
                } else {
                    sxssfExcelTask = createExcelTask(cls, startIndex, sheet, null,
                            excelExportConfig.getMapData().subList(startIndex, pageNum * perTaskLimit + lastCount));
                }
                futureSet.add(executorService.submit(sxssfExcelTask));
            }
            failFast(futureSet);
        } finally {
            executorService.shutdownNow();
        }
        return sxssfWorkbook;
    }

    /**
     * 阻塞线程,判断有无失败的,如有则停止所有线程不再往下执行,从第一个线程往下执行
     *
     * @param futureSet futureSet
     */
    private void failFast(Set<Future<Void>> futureSet) throws ExecutionException, InterruptedException {
        while (true) {
            if (excelExportConfig.isError()) {
                Set<Future<Void>> futureSet1 = new HashSet<>();
                for (Future<Void> future : futureSet) {
                    if (future.isDone()) {
                        futureSet1.add(future);
                    } else {
                        future.cancel(true);
                    }
                    log.error(future.isDone() + "\t" + future.isCancelled());
                }
                log.error("线程已全部终止！");
                for (Future<Void> future : futureSet1) {
                    future.get();
                }

            }
            boolean flag = true;
            for (Future<Void> future : futureSet) {
                if (!future.isDone()) {
                    flag = false;
                }
            }
            if (flag) {
                break;
            }
        }
    }

    /**
     * 创建ExcelTask
     *
     * @param cls        ExcelTask字节码
     * @param startIndex 开始下标
     * @param sheet      sheet
     * @param data       数据
     * @return ExcelTask
     * @throws IllegalAccessException 异常
     * @throws InstantiationException 异常
     */
    private AbstractSxssfExcelTask createExcelTask(Class<? extends AbstractSxssfExcelTask> cls, int startIndex,
                                                   SXSSFSheet sheet, List<List<Object>> data, List<Map<String, Object>> mapData) throws IllegalAccessException, InstantiationException {
        AbstractSxssfExcelTask sxssfExcelTask;
        if (cls != null) {
            sxssfExcelTask = cls.newInstance();
        } else {
            sxssfExcelTask = new DefaultSxssfSxssfExcelTask();
        }
        sxssfExcelTask.setStartIndex(startIndex);
        sxssfExcelTask.setSheet(sheet);
        sxssfExcelTask.setDataList(data);
        sxssfExcelTask.setMapDataList(mapData);
        sxssfExcelTask.setExcelExportConfig(excelExportConfig);
        return sxssfExcelTask;
    }


    /**
     * 导出基础配置
     *
     * @throws Exception 异常
     */
    private void writeConfig() throws Exception {

        // 检查参数配置信息

        if (DataUtil.isEmpty(excelExportConfig.getFileNamePrefix())) {
            excelExportConfig.setFileNamePrefix("报表");
            // throw new Exception("表名不能为NULL");
        }

        if (excelExportConfig.getColumnList() == null || excelExportConfig.getColumnList().size() == 0) {
            throw new Exception("列配置不能为空或者为NULL");
        }

        if (DataUtil.isEmpty(excelExportConfig.getFileNamePrefix())) {
            excelExportConfig.setFileNamePrefix("sheet");
            // throw new Exception("工作表表名不能为NULL");
        }

        if (DataUtil.isEmpty(excelExportConfig.getFileNameFormat())) {
            excelExportConfig.setFileNameFormat("yyyyMMddHHmmss");
            // throw new Exception("工作表表名不能为NULL");
        }

        if (excelExportConfig.isRowNum()) {
            excelExportConfig.getColumnList().add(0, new ExcelExportColumn("序号"));
        }
    }

    /**
     * 设置文件名称前缀
     *
     * @param fileNamePrefix fileNamePrefix
     */
    private void setFileNamePrefix(String fileNamePrefix) {
        excelExportConfig.setFileNamePrefix(fileNamePrefix);
    }

    /**
     * 基于Response返回Excel
     *
     * @param response 返回流
     */
    public void exportByResponse(HttpServletResponse response) {
        String fileName = excelExportConfig.getFileNamePrefix() + UNDERLINE + getStringDate(excelExportConfig.getFileNameFormat());
        ExcelUtil.exportByResponse(response, sxssfWorkbook, fileName);
    }

    /**
     * 导出到文件
     *
     * @param path 路径
     * @return 导出是否成功 返回文件名
     */
    public String exportExcelByFile(String path) {
        final String underline = "_";
        String fileName = excelExportConfig.getFileNamePrefix() + underline + getStringDate(excelExportConfig.getFileNameFormat()) + ".xlsx";
        ExcelUtil.exportByFile(sxssfWorkbook, path + File.separator + fileName);
        return fileName;
    }

}