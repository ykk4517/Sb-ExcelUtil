package com.ykkblog.excelutil.sxssf;

import com.ykkblog.excelutil.util.ExcelConstant;
import com.ykkblog.excelutil.util.ExcelExportColumn;
import com.ykkblog.excelutil.util.ExcelExportConfig;
import com.ykkblog.excelutil.util.ExcelUtil;
import com.ykkblog.fastbase.util.DataUtil;
import lombok.Data;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * SXSSFWorkbook渲染数据基类
 *
 * @author YKK
 * @date 2021/9/9 20:11
 **/
@Data
public abstract class AbstractSxssfExcelTask implements Callable<Void> {

    public SXSSFSheet sheet;
    public List<List<Object>> dataList;
    public List<Map<String, Object>> mapDataList;
    public ExcelExportConfig excelExportConfig;
    public List<Integer> columnWidth;
    public Integer startIndex = 0;
    DataFormatter formatter = new DataFormatter();
    public boolean defaultMode;
    public Integer dataSize;

    /**
     * Computes a result, or throws an exception if unable to do so.
     *
     * @return computed result
     */
    @Override
    public Void call() {
        try {
            defaultMode = mapDataList == null;
            dataSize = defaultMode ? dataList == null ? 0 : dataList.size() : mapDataList.size();
            init();
            operateTitle();
            operateTitleColumn();
            operateContent();
            if (excelExportConfig.getColumnList().stream().anyMatch(ExcelExportColumn::getCal)) {
                operateSum();
                operateDescription(3);
            } else {
                operateDescription(2);
            }
            operateColumnWidth();
        } catch (Exception e) {
            excelExportConfig.setError(true);
            throw e;
        }
        return null;
    }

    /**
     * 初始化操作,尽情定义你的东西吧
     */
    public abstract void init();

    /**
     * 渲染标题
     */
    private void operateTitle() {
        if (DataUtil.isNotBlank(excelExportConfig.getTitle())) {
            // 标题样式
            CellStyle titleCellStyle = ExcelUtil.getCellStyle(sheet.getWorkbook(), excelExportConfig.getTitleSize(),
                    true, false, ExcelConstant.ExcelColor.DEFAULT);
            SXSSFRow titleRow = sheet.createRow(0);
            // 行高
            titleRow.setHeightInPoints(30);
            titleRow.createCell(0).setCellValue(excelExportConfig.getTitle());

            // 合并单元格
            CellRangeAddress regionw;
            // 给定要合并的单元格范围
            regionw = new CellRangeAddress(0, 0, 0, excelExportConfig.getColumnList().size() - 1);
            // 这是合并单元格过程
            sheet.addMergedRegion(regionw);
            // 给合并过的单元格加边框
            ExcelUtil.setBorderStyle(BorderStyle.THIN, regionw, sheet);
            titleRow.getCell(0).setCellStyle(titleCellStyle);
            // 自定义函数去
            handleRow(titleRow, startIndex, 0);
        }
    }

    /**
     * 渲染列标题
     */
    private void operateTitleColumn() {
        // 列宽度
        columnWidth = new ArrayList<>(excelExportConfig.getColumnList().size());
        for (int i = 0; i < excelExportConfig.getColumnList().size(); i++) {
            columnWidth.add(i, 0);
        }

        // 列头样式
        CellStyle columnCellStyle = ExcelUtil.getCellStyle(sheet.getWorkbook(), excelExportConfig.getColumnSize(),
                false, false, ExcelConstant.ExcelColor.DEFAULT);
        CellStyle columnCellStyleWrap = ExcelUtil.getCellStyle(sheet.getWorkbook(), excelExportConfig.getColumnSize(),
                false, true, ExcelConstant.ExcelColor.DEFAULT);
        SXSSFRow columnRow = sheet.createRow(DataUtil.isNotBlank(excelExportConfig.getTitle()) ? 1 : 0);
        // 自定义函数去
        handleRow(columnRow, startIndex + (DataUtil.isNotBlank(excelExportConfig.getTitle()) ? 1 : 0), 1);
        for (int colIndex = 0; colIndex < excelExportConfig.getColumnList().size(); colIndex++) {
            columnRow.createCell(colIndex).setCellValue(excelExportConfig.getColumnList().get(colIndex).getName());

            if (excelExportConfig.getColumnList().get(colIndex).getAutoWrap() || excelExportConfig.getColumnList().get(colIndex).getColumnWidth() != null) {
                columnRow.getCell(colIndex).setCellStyle(columnCellStyleWrap);
            } else {
                columnRow.getCell(colIndex).setCellStyle(columnCellStyle);
                // 记录最大宽度
                ExcelUtil.getColumnWidth(formatter.formatCellValue(columnRow.getCell(colIndex)), colIndex, columnWidth);
            }
            // 用户自定义操作
            handleCell(columnRow.getCell(colIndex), colIndex, startIndex + (DataUtil.isNotBlank(excelExportConfig.getTitle()) ? 1 : 0), 1);
        }
    }

    /**
     * 渲染内容
     */
    private void operateContent() {
        SXSSFRow contentRow;
        Object obj;

        // 内容样式
        CellStyle contentCellStyle = ExcelUtil.getCellStyle(sheet.getWorkbook(), excelExportConfig.getContentSize(),
                false, false, ExcelConstant.ExcelColor.DEFAULT);
        CellStyle contentCellStyleWrap = ExcelUtil.getCellStyle(sheet.getWorkbook(), excelExportConfig.getContentSize(),
                false, true, ExcelConstant.ExcelColor.DEFAULT);

        int showNum = excelExportConfig.isRowNum() ? 1 : 0;

        for (int dataRowIndex = 0; dataRowIndex < dataSize; dataRowIndex++) {

            int rowIndex = dataRowIndex + (DataUtil.isNotBlank(excelExportConfig.getTitle()) ? 2 : 1);
            // 具体每一行的数据
            contentRow = sheet.createRow(rowIndex);

            // 自定义函数去
            handleRow(contentRow, rowIndex, 2);

            // 遍历每一列
            for (int colIndex = 0; colIndex < excelExportConfig.getColumnList().size(); colIndex++) {
                // 如果显示序号
                if (colIndex == 0 && excelExportConfig.isRowNum()) {
                    contentRow.createCell(0).setCellValue(startIndex + dataRowIndex + 1);
                    obj = startIndex + dataRowIndex + 1;
                } else {
                    obj = defaultMode ? dataList.get(dataRowIndex).get(colIndex - showNum) :
                            mapDataList.get(dataRowIndex).get(excelExportConfig.getColumnList().get(colIndex).getKey());
                    if (DataUtil.isEmpty(obj)) {
                        contentRow.createCell(colIndex).setCellValue("");
                    } else {
                        if (obj instanceof String) {
                            if ("1000010".equals(String.valueOf(obj))) {
                                //int a = 1 / 0;
                                System.out.println("666");
                                //throw new BusinessException("ase");
                            }
                            contentRow.createCell(colIndex).setCellValue(obj.toString());
                        } else {
                            if (obj instanceof Date) {
                                contentRow.createCell(colIndex).setCellValue((Date) obj);
                            } else {
                                contentRow.createCell(colIndex).setCellValue(Double.parseDouble(String.valueOf(obj)));
                            }
                        }
                    }
                }

                if (excelExportConfig.getColumnList().get(colIndex).getAutoWrap() || excelExportConfig.getColumnList().get(colIndex).getColumnWidth() != null) {
                    contentRow.getCell(colIndex).setCellStyle(contentCellStyleWrap);
                } else {
                    contentRow.getCell(colIndex).setCellStyle(contentCellStyle);
                    // 记录最大宽度
                    ExcelUtil.getColumnWidth(formatter.formatCellValue(contentRow.getCell(colIndex)), colIndex, columnWidth);
                }

                // 用户自定义操作
                handleCell(contentRow.getCell(colIndex), colIndex, obj, 2);
            }
        }
    }

    /**
     * 渲染合计行
     */
    private void operateSum() {
        // 合计行
        CellStyle contentCellStyle = ExcelUtil.getCellStyle(sheet.getWorkbook(), excelExportConfig.getContentSize(),
                false, false, ExcelConstant.ExcelColor.DEFAULT);

        boolean hasSumRow = false;
        int sumRowIndex = 2 + dataSize;
        SXSSFRow sumRow = sheet.createRow(sumRowIndex);
        SXSSFCell sumCell;
        // 行高
        sumRow.setHeightInPoints(20);
        DataFormatter formatter = new DataFormatter();
        for (int colIndex = 0; colIndex < excelExportConfig.getColumnList().size(); colIndex++) {
            if (!excelExportConfig.getColumnList().get(colIndex).getCal()) {
                sumRow.createCell(colIndex).setCellStyle(contentCellStyle);
                continue;
            }
            // 是否已设置
            if (!hasSumRow && colIndex > 1) {
                sumCell = sumRow.createCell(0);
                CellRangeAddress region = new CellRangeAddress(sumRowIndex, sumRowIndex, 0, colIndex - 1);
                // 这是合并单元格过程
                sheet.addMergedRegion(region);
                // 给合并过的单元格加边框
                ExcelUtil.setBorderStyle(BorderStyle.THIN, region, sheet);
                sumCell.setCellValue("合计：");
                sumCell.setCellStyle(contentCellStyle);
                hasSumRow = true;
            }
            sumCell = sumRow.createCell(colIndex);
            sumCell.setCellType(CellType.FORMULA);
            sumCell.setCellFormula("SUM(" + ExcelUtil.indexToColumn(colIndex) + 3 + ":"
                    + ExcelUtil.indexToColumn(colIndex) + sumRowIndex + ")");
            /*FormulaEvaluator formulaEvaluator = sxssfWorkbook.getCreationHelper().createFormulaEvaluator();
            formulaEvaluator.evaluateInCell(cell);*/
            sumCell.setCellStyle(contentCellStyle);
            // 记录最大宽度
            ExcelUtil.getColumnWidth(formatter.formatCellValue(sumRow.getCell(colIndex)), colIndex, columnWidth);
        }
        if (!hasSumRow) {
            sumCell = sumRow.createCell(0);
            sumCell.setCellValue("合计：");
            sumCell.setCellStyle(contentCellStyle);
            // 记录最大宽度
            ExcelUtil.getColumnWidth(formatter.formatCellValue(sumRow.getCell(0)), 0, columnWidth);
        }
    }

    /**
     * 调整列宽
     */
    private void operateColumnWidth() {
        // 调整列宽
        sheet.trackAllColumnsForAutoSizing();
        //  && dataList.get(0).size() > 0
        if (dataSize > 0) {
            for (int colIndex = 0; colIndex < excelExportConfig.getColumnList().size(); colIndex++) {
                // 设置宽度
                // 默认2048
                // System.out.println(colIndex + "<- " + sheet.getColumnWidth(colIndex));
                sheet.autoSizeColumn(colIndex);
                if (excelExportConfig.getColumnList().get(colIndex).getColumnWidth() != null) {
                    // sheet.setColumnWidth(colIndex, excelExportConfig.getColumnList().get(colIndex).getColumnWidth());
                    sheet.setColumnWidth(colIndex, new Float(excelExportConfig.getColumnList().get(colIndex).getColumnWidth() * 33.5).intValue());
                } else if (!excelExportConfig.getColumnList().get(colIndex).getAutoWrap()) {
                    sheet.setColumnWidth(colIndex, columnWidth.get(colIndex));
                }
            }
        }
        // 冻结列行
        sheet.createFreezePane(excelExportConfig.getFreezeColumnNum(), 2, excelExportConfig.getFreezeColumnNum(), 2);
    }

    /**
     * 渲染报表描述
     *
     * @param offsetIndex 起始预留行
     */
    private void operateDescription(Integer offsetIndex) {
        // 最后描述
        if (DataUtil.isEmpty(excelExportConfig.getDescription())) {
            return;
        }

        CellStyle descriptionCellStyle = ExcelUtil.getCellStyle(sheet.getWorkbook(), excelExportConfig.getContentSize(),
                false, false, ExcelConstant.ExcelColor.DEFAULT);
        descriptionCellStyle.setAlignment(HorizontalAlignment.LEFT);
        descriptionCellStyle.setWrapText(true);

        // 封装最后一个 描述    创建rows  (表描述)  预留了合计行
        SXSSFRow descriptionRow = sheet.createRow(dataSize + offsetIndex);
        descriptionRow.setHeightInPoints(100);

        SXSSFCell descriptionCell = descriptionRow.createCell(0);
        descriptionCell.setCellValue(excelExportConfig.getDescription());
        descriptionCell.setCellStyle(descriptionCellStyle);

        CellRangeAddress region;
        if (excelExportConfig.isRowNum()) {
            // 合并单元格
            // 给定要合并的单元格范围
            region = new CellRangeAddress(dataSize + offsetIndex, dataSize + offsetIndex, 0, excelExportConfig.getColumnList().size());
        } else {
            // 合并单元格
            // 给定要合并的单元格范围
            region = new CellRangeAddress(dataSize + offsetIndex, dataSize + offsetIndex, 0, excelExportConfig.getColumnList().size() - 1);
        }

        // 这是合并单元格过程
        sheet.addMergedRegion(region);
        // 给合并过的单元格加边框
        ExcelUtil.setBorderStyle(BorderStyle.THIN, region, sheet);

    }

    /**
     * 渲染数据核心处理代码
     *
     * @param contentRow 当前行
     * @param rowIndex   当前行下标
     * @param rowType    当前行类型 0 标题；1 列名；2 内容
     */
    public abstract void handleRow(SXSSFRow contentRow, int rowIndex, Integer rowType);

    /**
     * 渲染数据核心处理代码
     *
     * @param contentCell 当前列
     * @param colIndex    当前列下标
     * @param obj         数据值
     * @param rowType     当前行类型 0 标题；1 列名；2 内容
     */
    public abstract void handleCell(SXSSFCell contentCell, int colIndex, Object obj, Integer rowType);

    /**
     * 同步创建行
     *
     * @param index 行下标
     * @return 行对象
     */
    public synchronized SXSSFRow createRow(Integer index) {
        return sheet.createRow(index);
    }

}