package com.ykkblog.excelutil.xlsx2html;

import com.ykkblog.excelutil.util.PictureDataUtil;
import com.ykkblog.fastbase.util.ConstantBase;
import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.hwpf.converter.HtmlDocumentFacade;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.Beta;
import org.apache.poi.util.POILogFactory;
import org.apache.poi.util.POILogger;
import org.apache.poi.util.XMLHelper;
import org.apache.poi.xssf.usermodel.*;
import org.openxmlformats.schemas.drawingml.x2006.spreadsheetDrawing.CTMarker;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Converts xlsx files (07-19) to HTML file.
 *
 * @author Sergey Vladimirov (vlsergey {at} gmail {dot} com)
 */
@Beta
public class XlsxToHtmlConverter extends AbstractXlsxConverter {

    private static final POILogger LOGGER = POILogFactory.getLogger(XlsxToHtmlConverter.class);
    private FormulaEvaluator formulaEvaluator;

    public XlsxToHtmlConverter() throws ParserConfigurationException {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        htmlDocumentFacade = new HtmlDocumentFacade(document);
    }

    /**
     * Java main() interface to interact with {@link XlsxToHtmlConverter}
     *
     * <p>
     * Usage: XlsxToHtmlConverter infile outfile
     * </p>
     * Where infile is an input .xlsx file ( Word 07-19) which will be rendered
     * as HTML into outfile
     *
     * @throws TransformerException TransformerException
     */
    public static void main(String[] args)
            throws IOException, ParserConfigurationException, TransformerException {
        args = new String[]{"C:\\Users\\Administrator\\Desktop\\新建 Microsoft Excel 工作表 (2).xlsx", "C:\\Users\\Administrator\\Desktop\\test123.html"};
        if (args.length < ConstantBase.BasicNumber.TWO) {
            System.err.println("Usage: XlsxToHtmlConverter <inputFile.xlsx> <saveTo.html>");
            return;
        }

        System.out.println("Converting " + args[0]);
        System.out.println("Saving output to " + args[1]);

        XlsxToHtmlConverter xlsxToHtmlConverter = new XlsxToHtmlConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());

        Document doc = xlsxToHtmlConverter.process(new File(args[0]));


        DOMSource domSource = new DOMSource(doc);
        StreamResult streamResult = new StreamResult(new File(args[1]));

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer serializer = tf.newTransformer();
        // TODO set encoding from a command argument
        serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        serializer.setOutputProperty(OutputKeys.INDENT, "no");
        serializer.setOutputProperty(OutputKeys.METHOD, "html");
        serializer.transform(domSource, streamResult);
    }

    /**
     * Converts Excel file ( Word 07-19) into HTML file.
     *
     * @param xlsxFile workbook file to process
     * @return DOM representation of result HTML
     * @throws IOException IOException
     */
    public Document process(File xlsxFile) throws IOException {
        try (XSSFWorkbook workbook = XlsxToHtmlUtils.loadXlsx(xlsxFile)) {
            //return process(workbook);
            processWorkbook(workbook);
            return getDocument();
        }
    }

    /**
     * Converts Excel file ( Word 07-19) into HTML file.
     *
     * @param xlsStream workbook stream to process
     * @return DOM representation of result HTML
     * @throws IOException                  IOException
     * @throws ParserConfigurationException ParserConfigurationException
     */
    public Document process(InputStream xlsStream) throws IOException, ParserConfigurationException {
        try (XSSFWorkbook workbook = new XSSFWorkbook(xlsStream)) {
            return process(workbook);
        }
    }

    /**
     * Converts Excel file ( Word 07-19) into HTML file.
     *
     * @param workbook workbook instance to process
     * @return DOM representation of result HTML
     * @throws ParserConfigurationException ParserConfigurationException
     */
    public Document process(XSSFWorkbook workbook) throws ParserConfigurationException {
        XlsxToHtmlConverter xlsxToHtmlConverter = new XlsxToHtmlConverter(
                XMLHelper.getDocumentBuilderFactory().newDocumentBuilder().newDocument());
        xlsxToHtmlConverter.processWorkbook(workbook);
        return xlsxToHtmlConverter.getDocument();
    }

    private String cssClassContainerCell = null;

    private String cssClassContainerDiv = null;

    private String cssClassPrefixCell = "c";

    private String cssClassPrefixDiv = "d";

    private String cssClassPrefixRow = "r";

    private String cssClassPrefixTable = "t";

    private final Map<Short, String> excelStyleToClass = new LinkedHashMap<>();

    private final HtmlDocumentFacade htmlDocumentFacade;

    private boolean useDivsToSpan = false;

    public XlsxToHtmlConverter(Document doc) {
        htmlDocumentFacade = new HtmlDocumentFacade(doc);
    }

    public XlsxToHtmlConverter(HtmlDocumentFacade htmlDocumentFacade) {
        this.htmlDocumentFacade = htmlDocumentFacade;
    }

    protected String buildStyle(XSSFCellStyle cellStyle) {
        StringBuilder style = new StringBuilder();

        style.append("white-space:pre-wrap;");
        XlsxToHtmlUtils.appendAlign(style, cellStyle.getAlignmentEnum());

        switch (cellStyle.getFillPatternEnum()) {
            // no fill
            case NO_FILL:
                break;
            case SOLID_FOREGROUND:
                if (cellStyle.getFillForegroundColorColor() != null) {
                    style.append("background-color:").append(XlsxToHtmlUtils.getColor(cellStyle.getFillForegroundColorColor())).append(";");
                }
                break;
            default:
                if (cellStyle.getFillBackgroundColorColor() != null) {
                    style.append("background-color:").append(XlsxToHtmlUtils.getColor(cellStyle.getFillBackgroundColorColor())).append(";");
                }
                break;
        }

        buildStyle_border(style, "top", cellStyle.getBorderTopEnum(),
                cellStyle.getTopBorderXSSFColor());
        // workbook getRightBorderXSSFColor
        buildStyle_border(style, "right",
                cellStyle.getBorderRightEnum(), cellStyle.getRightBorderXSSFColor());
        buildStyle_border(style, "bottom",
                cellStyle.getBorderBottomEnum(), cellStyle.getBottomBorderXSSFColor());
        buildStyle_border(style, "left", cellStyle.getBorderLeftEnum(),
                cellStyle.getLeftBorderXSSFColor());

        XSSFFont font = cellStyle.getFont();
        buildStyle_font(style, font);

        return style.toString();
    }

    private void buildStyle_border(StringBuilder style, String type, BorderStyle xlsxBorder, XSSFColor borderColor) {

        if (xlsxBorder == BorderStyle.NONE) {
            return;
        }

        StringBuilder borderStyle = new StringBuilder();
        borderStyle.append(XlsxToHtmlUtils.getBorderWidth(xlsxBorder));
        borderStyle.append(' ');
        borderStyle.append(XlsxToHtmlUtils.getBorderStyle(xlsxBorder));

        borderStyle.append(' ');
        if (borderColor == null || borderColor.getARGBHex() == null) {
            borderStyle.append("#000000");
        } else {
            borderStyle.append(XlsxToHtmlUtils.getColor(borderColor));
        }

        style.append("border-").append(type).append(":").append(borderStyle).append(";");
    }

    void buildStyle_font(StringBuilder style, XSSFFont font) {
        if (font.getBold()) {
            style.append("font-weight:bold;");
        }

        if (font.getXSSFColor() != null) {
            style.append("color: ").append(XlsxToHtmlUtils.getColor(font.getXSSFColor())).append("; ");
        }

        if (font.getFontHeightInPoints() != 0) {
            style.append("font-size:").append(font.getFontHeightInPoints()).append("pt;");
        }

        if (font.getItalic()) {
            style.append("font-style:italic;");
        }
    }

    public String getCssClassPrefixCell() {
        return cssClassPrefixCell;
    }

    public String getCssClassPrefixDiv() {
        return cssClassPrefixDiv;
    }

    public String getCssClassPrefixRow() {
        return cssClassPrefixRow;
    }

    public String getCssClassPrefixTable() {
        return cssClassPrefixTable;
    }

    @Override
    public Document getDocument() {
        return htmlDocumentFacade.getDocument();
    }

    protected String getStyleClassName(XSSFCellStyle cellStyle) {
        final Short cellStyleKey = cellStyle.getIndex();

        String knownClass = excelStyleToClass.get(cellStyleKey);
        if (knownClass != null) {
            return knownClass;
        }

        String cssStyle = buildStyle(cellStyle);
        String cssClass = htmlDocumentFacade.getOrCreateCssClass(cssClassPrefixCell, cssStyle);
        excelStyleToClass.put(cellStyleKey, cssClass);
        return cssClass;
    }

    public boolean isUseDivsToSpan() {
        return useDivsToSpan;
    }

    protected boolean processCell(XSSFCell cell, Element tableCellElement,
                                  int normalWidthPx, int maxSpannedWidthPx, float normalHeightPt, Element image) {
        final XSSFCellStyle cellStyle = cell.getCellStyle();

        String value;
        switch (cell.getCellTypeEnum()) {
            case STRING:
                // XXX: enrich
                value = cell.getRichStringCellValue().getString();
                break;
            case FORMULA:
                switch (cell.getCachedFormulaResultTypeEnum()) {
                    case STRING:
                        XSSFRichTextString str = cell.getRichStringCellValue();
                        if (str.length() > 0) {
                            value = (str.toString());
                        } else {
                            value = XlsxToHtmlUtils.EMPTY;
                        }
                        break;
                    case NUMERIC:
                        // double nValue = cell.getNumericCellValue();
                        CellValue cellValue = formulaEvaluator.evaluate(cell);
                        short df = cellStyle.getDataFormat();
                        String dfs = cellStyle.getDataFormatString();
                        value = formatter.formatRawCellContents(cellValue.getNumberValue(), df, dfs);
                        break;
                    case BOOLEAN:
                        value = String.valueOf(cell.getBooleanCellValue());
                        break;
                    case ERROR:
                        value = ErrorEval.getText(cell.getErrorCellValue());
                        break;
                    default:
                        LOGGER.log(
                                POILogger.WARN,
                                "Unexpected cell cachedFormulaResultType ("
                                        + cell.getCachedFormulaResultTypeEnum() + ")");
                        value = XlsxToHtmlUtils.EMPTY;
                        break;
                }
                break;
            case BLANK:
                value = XlsxToHtmlUtils.EMPTY;
                break;
            case NUMERIC:
                value = formatter.formatCellValue(cell);
                break;
            case BOOLEAN:
                value = String.valueOf(cell.getBooleanCellValue());
                break;
            case ERROR:
                value = ErrorEval.getText(cell.getErrorCellValue());
                break;
            default:
                LOGGER.log(POILogger.WARN,
                        "Unexpected cell type (" + cell.getCellTypeEnum() + ")");
                return true;
        }

        final boolean noText = XlsxToHtmlUtils.isEmpty(value);
        final boolean wrapInDivs = !noText && isUseDivsToSpan() && !cellStyle.getWrapText();

        if (cellStyle.getIndex() != 0) {
            @SuppressWarnings("resource")
            // XSSFWorkbook workbook = cell.getRow().getSheet().getWorkbook();
            String mainCssClass = getStyleClassName(cellStyle);

            if (wrapInDivs) {
                tableCellElement.setAttribute("class", mainCssClass + " " + cssClassContainerCell);
            } else {
                tableCellElement.setAttribute("class", mainCssClass);
            }

            if (noText) {
                /*
                 * if cell style is defined (like borders, etc.) but cell text
                 * is empty, add "&nbsp;" to output, so browser won't collapse
                 * and ignore cell
                 */
                value = "\u00A0";
            }
        }

        if (isOutputLeadingSpacesAsNonBreaking() && value.startsWith(" ")) {
            StringBuilder builder = new StringBuilder();
            for (int c = 0; c < value.length(); c++) {
                if (value.charAt(c) != ' ') {
                    break;
                }
                builder.append('\u00a0');
            }

            if (value.length() != builder.length()) {
                builder.append(value.substring(builder.length()));
            }

            value = builder.toString();
        }

        Text text = htmlDocumentFacade.createText(value);

        if (wrapInDivs) {
            Element outerDiv = htmlDocumentFacade.createBlock();
            outerDiv.setAttribute("class", this.cssClassContainerDiv);

            Element innerDiv = htmlDocumentFacade.createBlock();
            StringBuilder innerDivStyle = new StringBuilder();
            innerDivStyle.append("position:absolute;min-width:");
            innerDivStyle.append(normalWidthPx);
            innerDivStyle.append("px;");
            if (maxSpannedWidthPx != Integer.MAX_VALUE) {
                innerDivStyle.append("max-width:");
                innerDivStyle.append(maxSpannedWidthPx);
                innerDivStyle.append("px;");
            }
            innerDivStyle.append("overflow:hidden;max-height:");
            innerDivStyle.append(normalHeightPt);
            innerDivStyle.append("pt;white-space:nowrap;");
            // getAlignment
            XlsxToHtmlUtils.appendAlign(innerDivStyle, cellStyle.getAlignmentEnum());
            htmlDocumentFacade.addStyleClass(outerDiv, cssClassPrefixDiv, innerDivStyle.toString());

            if (!"\u00A0".equals(text.getData()) || image == null) {
                innerDiv.appendChild(text);
            }
            if (image != null) {
                innerDiv.appendChild(image);
            }
            outerDiv.appendChild(innerDiv);
            tableCellElement.appendChild(outerDiv);
        } else {
            // https://blog.csdn.net/y_j_zhang/article/details/102494319
            if (image != null) {
                tableCellElement.appendChild(image);
            }
            if (!"\u00A0".equals(text.getData()) || image == null) {
                tableCellElement.appendChild(text);
            }
        }
        return XlsxToHtmlUtils.isEmpty(value) && (cellStyle.getIndex() == 0);
    }

    protected void processColumnHeaders(XSSFSheet sheet, int maxSheetColumns, Element table) {
        Element tableHeader = htmlDocumentFacade.createTableHeader();
        table.appendChild(tableHeader);

        Element tr = htmlDocumentFacade.createTableRow();

        if (isOutputRowNumbers()) {
            // empty row at left-top corner
            tr.appendChild(htmlDocumentFacade.createTableHeaderCell());
        }

        for (int c = 0; c < maxSheetColumns; c++) {
            if (!isOutputHiddenColumns() && sheet.isColumnHidden(c)) {
                continue;
            }

            Element th = htmlDocumentFacade.createTableHeaderCell();
            String text = getColumnName(c);
            th.appendChild(htmlDocumentFacade.createText(text));
            tr.appendChild(th);
        }
        tableHeader.appendChild(tr);
    }

    /**
     * Creates COLGROUP element with width specified for all columns. (Except
     * first if <tt>{@link #isOutputRowNumbers()}==true</tt>)
     */
    protected void processColumnWidths(XSSFSheet sheet, int maxSheetColumns, Element table) {
        // draw COLS after we know max column number
        Element columnGroup = htmlDocumentFacade.createTableColumnGroup();
        if (isOutputRowNumbers()) {
            columnGroup.appendChild(htmlDocumentFacade.createTableColumn());
        }
        for (int c = 0; c < maxSheetColumns; c++) {
            if (!isOutputHiddenColumns() && sheet.isColumnHidden(c)) {
                continue;
            }

            Element col = htmlDocumentFacade.createTableColumn();
            col.setAttribute("width", String.valueOf(getColumnWidth(sheet, c)));
            columnGroup.appendChild(col);
        }
        table.appendChild(columnGroup);
    }

    protected void processCoreProps(POIXMLProperties.CoreProperties coreProperties) {
        if (XlsxToHtmlUtils.isNotEmpty(coreProperties.getTitle())) {
            htmlDocumentFacade.setTitle(coreProperties.getTitle());
        }

        if (XlsxToHtmlUtils.isNotEmpty(coreProperties.getCreator())) {
            // htmlDocumentFacade.addAuthor(coreProperties.getCreator());
        }
        htmlDocumentFacade.addAuthor("com.ykkblog.PoiUtil");

        if (XlsxToHtmlUtils.isNotEmpty(coreProperties.getKeywords())) {
            htmlDocumentFacade.addKeywords(coreProperties.getKeywords());
        }

        if (XlsxToHtmlUtils.isNotEmpty(coreProperties.getSubject())) {
            htmlDocumentFacade.addDescription(coreProperties.getSubject());
        }
    }

    /**
     * @return maximum 1-base index of column that were rendered, zero if none
     */
    protected int processRow(CellRangeAddress[][] mergedRanges, XSSFRow row, Element tableRowElement, Map<String, XSSFPictureData> xssfPictureDataMap) {
        final XSSFSheet sheet = row.getSheet();
        final short maxColIx = row.getLastCellNum();
        if (maxColIx <= 0) {
            return 0;
        }

        final List<Element> emptyCells = new ArrayList<>(maxColIx);

        if (isOutputRowNumbers()) {
            Element tableRowNumberCellElement = htmlDocumentFacade.createTableHeaderCell();
            processRowNumber(row, tableRowNumberCellElement);
            emptyCells.add(tableRowNumberCellElement);
        }

        int maxRenderedColumn = 0;
        for (int colIx = 0; colIx < maxColIx; colIx++) {
            if (!isOutputHiddenColumns() && sheet.isColumnHidden(colIx)) {
                continue;
            }

            CellRangeAddress range = XlsxToHtmlUtils.getMergedRange(mergedRanges, row.getRowNum(), colIx);

            if (range != null && (range.getFirstColumn() != colIx || range.getFirstRow() != row.getRowNum())) {
                continue;
            }

            XSSFCell cell = row.getCell(colIx);

            int divWidthPx = 0;
            if (isUseDivsToSpan()) {
                divWidthPx = getColumnWidth(sheet, colIx);

                boolean hasBreaks = false;
                for (int nextColumnIndex = colIx + 1; nextColumnIndex < maxColIx; nextColumnIndex++) {
                    if (!isOutputHiddenColumns() && sheet.isColumnHidden(nextColumnIndex)) {
                        continue;
                    }

                    if (row.getCell(nextColumnIndex) != null && !isTextEmpty(row.getCell(nextColumnIndex))) {
                        hasBreaks = true;
                        break;
                    }

                    divWidthPx += getColumnWidth(sheet, nextColumnIndex);
                }

                if (!hasBreaks) {
                    divWidthPx = Integer.MAX_VALUE;
                }
            }

            Element tableCellElement = htmlDocumentFacade.createTableCell();

            if (range != null) {
                if (range.getFirstColumn() != range.getLastColumn()) {
                    tableCellElement.setAttribute("colspan", String.valueOf(range.getLastColumn() - range.getFirstColumn() + 1));
                }
                if (range.getFirstRow() != range.getLastRow()) {
                    tableCellElement.setAttribute("rowspan", String.valueOf(range.getLastRow() - range.getFirstRow() + 1));
                }
            }

            boolean emptyCell;
            if (cell != null) {
                Element image = null;
                if (xssfPictureDataMap.containsKey(row.getRowNum() + "_" + colIx)) {
                    image = htmlDocumentFacade.createImage("data:image/png;base64,"
                            + PictureDataUtil.pictureData2Base64(xssfPictureDataMap.get(row.getRowNum() + "_" + colIx)));
                }
                emptyCell = processCell(cell, tableCellElement, getColumnWidth(sheet, colIx), divWidthPx, row.getHeight() / 20f, image);
            } else {
                emptyCell = true;
            }

            if (emptyCell) {
                emptyCells.add(tableCellElement);
            } else {
                for (Element emptyCellElement : emptyCells) {
                    tableRowElement.appendChild(emptyCellElement);
                }
                emptyCells.clear();

                tableRowElement.appendChild(tableCellElement);
                maxRenderedColumn = colIx;
            }
        }

        return maxRenderedColumn + 1;
    }

    protected void processRowNumber(XSSFRow row, Element tableRowNumberCellElement) {
        tableRowNumberCellElement.setAttribute("class", "rownumber");
        Text text = htmlDocumentFacade.createText(getRowName(row));
        tableRowNumberCellElement.appendChild(text);
    }

    /**
     * @param sheet            sheet
     * @param renderStartIndex 转换开始下标
     * @param renderEndIndex   转换结束下标
     */
    protected void processSheet(XSSFSheet sheet, Integer renderStartIndex, Integer renderEndIndex) {
        // 输出sheetName
        if (isOutputSheetName()) {
            processSheetHeader(htmlDocumentFacade.getBody(), sheet);
        }

        final int physicalNumberOfRows = sheet.getPhysicalNumberOfRows();
        if (physicalNumberOfRows <= 0) {
            return;
        }

        // 提前获取到所有的图片
        Map<String, XSSFPictureData> xssfPictureDataMap = getPictrues(sheet);

        Element table = htmlDocumentFacade.createTable();
        htmlDocumentFacade.addStyleClass(table, cssClassPrefixTable, "border-collapse:collapse;border-spacing:0;");

        Element tableBody = htmlDocumentFacade.createTableBody();

        final CellRangeAddress[][] mergedRanges = XlsxToHtmlUtils.buildMergedRangesMap(sheet);

        final List<Element> emptyRowElements = new ArrayList<>(physicalNumberOfRows);
        int maxSheetColumns = 1;
        if (renderStartIndex == -1) {
            renderStartIndex = sheet.getFirstRowNum();
        }
        if (renderEndIndex == -1) {
            renderEndIndex = sheet.getLastRowNum();
        }
        for (int r = renderStartIndex; r <= renderEndIndex; r++) {
            XSSFRow row = sheet.getRow(r);

            if (row == null) {
                continue;
            }

            if (!isOutputHiddenRows() && row.getZeroHeight()) {
                continue;
            }

            Element tableRowElement = htmlDocumentFacade.createTableRow();
            htmlDocumentFacade.addStyleClass(tableRowElement, cssClassPrefixRow, "height:" + (row.getHeight() / 20f) + "pt;");

            int maxRowColumnNumber = processRow(mergedRanges, row, tableRowElement, xssfPictureDataMap);

            if (maxRowColumnNumber == 0) {
                emptyRowElements.add(tableRowElement);
            } else {
                if (!emptyRowElements.isEmpty()) {
                    for (Element emptyRowElement : emptyRowElements) {
                        tableBody.appendChild(emptyRowElement);
                    }
                    emptyRowElements.clear();
                }

                tableBody.appendChild(tableRowElement);
            }
            maxSheetColumns = Math.max(maxSheetColumns, maxRowColumnNumber);
        }

        processColumnWidths(sheet, maxSheetColumns, table);

        if (isOutputColumnHeaders()) {
            processColumnHeaders(sheet, maxSheetColumns, table);
        }

        table.appendChild(tableBody);

        htmlDocumentFacade.getBody().appendChild(table);
    }

    /**
     * 获取图片和位置 (xlsx)
     *
     * @param sheet sheet
     * @return Map<String, XSSFPictureData>
     */
    private Map<String, XSSFPictureData> getPictrues(XSSFSheet sheet) {
        Map<String, XSSFPictureData> map = new HashMap<>(16);
        List<POIXMLDocumentPart> list = sheet.getRelations();
        for (POIXMLDocumentPart part : list) {
            if (part instanceof XSSFDrawing) {
                XSSFDrawing drawing = (XSSFDrawing) part;
                List<XSSFShape> shapes = drawing.getShapes();
                for (XSSFShape shape : shapes) {
                    XSSFPicture picture = (XSSFPicture) shape;
                    XSSFClientAnchor anchor = picture.getPreferredSize();
                    CTMarker marker = anchor.getFrom();
                    System.out.println(marker.getRowOff());
                    System.out.println(marker.getColOff());
                    // 键格式：sheet索引_行号_列号_单元格内的上边距_单元格内的左边距_uuid
                    String key = marker.getRow() + "_" + marker.getCol();
                    map.put(key, picture.getPictureData());
                }
            }
        }
        return map;
    }

    protected void processSheetHeader(Element htmlBody, XSSFSheet sheet) {
        Element h2 = htmlDocumentFacade.createHeader2();
        h2.appendChild(htmlDocumentFacade.createText(sheet.getSheetName()));
        htmlBody.appendChild(h2);
    }

    public void processWorkbook(XSSFWorkbook workbook) {
        processWorkbook(workbook, -1, -1);
    }

    public void processWorkbook(XSSFWorkbook workbook, Integer renderStartIndex, Integer renderEndIndex) {
        // final SummaryInformation summaryInformation = workbook.getSummaryInformation();

        this.formulaEvaluator = workbook.getCreationHelper().createFormulaEvaluator();

        POIXMLProperties xmlProps = workbook.getProperties();
        POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();

        if (coreProps != null) {
            processCoreProps(coreProps);
        }

        if (isUseDivsToSpan()) {
            // prepare CSS classes for later usage
            this.cssClassContainerCell = htmlDocumentFacade.getOrCreateCssClass(cssClassPrefixCell, "padding:0;margin:0;align:left;vertical-align:top;");
            this.cssClassContainerDiv = htmlDocumentFacade.getOrCreateCssClass(cssClassPrefixDiv, "position:relative;");
        }

        for (int s = 0; s < workbook.getNumberOfSheets(); s++) {
            XSSFSheet sheet = workbook.getSheetAt(s);
            processSheet(sheet, renderStartIndex, renderEndIndex);
        }

        htmlDocumentFacade.updateStylesheet();
    }

    public void setCssClassPrefixCell(String cssClassPrefixCell) {
        this.cssClassPrefixCell = cssClassPrefixCell;
    }

    public void setCssClassPrefixDiv(String cssClassPrefixDiv) {
        this.cssClassPrefixDiv = cssClassPrefixDiv;
    }

    public void setCssClassPrefixRow(String cssClassPrefixRow) {
        this.cssClassPrefixRow = cssClassPrefixRow;
    }

    public void setCssClassPrefixTable(String cssClassPrefixTable) {
        this.cssClassPrefixTable = cssClassPrefixTable;
    }

    /**
     * Allows converter to wrap content into two additional DIVs with tricky
     * styles, so it will wrap across empty cells (like in Excel).
     * <p>
     * <b>Warning:</b> after enabling this mode do not serialize result HTML
     * with INDENT=YES option, because line breaks will make additional
     * (unwanted) changes
     */
    public void setUseDivsToSpan(boolean useDivsToSpan) {
        this.useDivsToSpan = useDivsToSpan;
    }
}
