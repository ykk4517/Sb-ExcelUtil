package com.ykkblog.excelutil.xlsx2html;


import lombok.Data;
import org.apache.poi.hssf.usermodel.HSSFDataFormatter;
import org.apache.poi.hwpf.converter.DefaultFontReplacer;
import org.apache.poi.hwpf.converter.FontReplacer;
import org.apache.poi.hwpf.converter.NumberFormatter;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.util.Beta;
import org.apache.poi.xssf.usermodel.*;
import org.w3c.dom.Document;

/**
 * Common class for {@link org.apache.poi.hssf.converter.ExcelToFoConverter} and {@link XlsxToHtmlConverter}
 *
 * @author Sergey Vladimirov (vlsergey {at} gmail {dot} com)
 * @see org.apache.poi.hwpf.converter.AbstractWordConverter
 */
@Beta
@Data
public abstract class AbstractXlsxConverter {
    protected static int getColumnWidth(XSSFSheet sheet, int columnIndex) {
        return XlsxToHtmlUtils.getColumnWidthInPx(sheet
                .getColumnWidth(columnIndex));
    }

    protected static int getDefaultColumnWidth(XSSFSheet sheet) {
        return XlsxToHtmlUtils.getColumnWidthInPx(sheet
                .getDefaultColumnWidth());
    }

    protected final HSSFDataFormatter formatter = new HSSFDataFormatter();

    private FontReplacer fontReplacer = new DefaultFontReplacer();

    /**
     * 是否输出列头ABCDE
     */
    private boolean outputSheetName = false;

    /**
     * 是否输出列头ABCDE
     */
    private boolean outputColumnHeaders = false;

    /**
     * 是否输出隐藏列
     */
    private boolean outputHiddenColumns = false;

    /**
     * 是否输出隐藏行
     */
    private boolean outputHiddenRows = false;

    /**
     * 这个不懂
     */
    private boolean outputLeadingSpacesAsNonBreaking = true;

    /**
     * 是否显示行号
     */
    private boolean outputRowNumbers = false;

    /**
     * Generates name for output as column header in case
     * <tt>{@link #isOutputColumnHeaders()} == true</tt>
     *
     * @param columnIndex 0-based column index
     */
    protected String getColumnName(int columnIndex) {
        return NumberFormatter.getNumber(columnIndex + 1, 3);
    }

    /**
     * getDocument
     *
     * @return Document
     */
    protected abstract Document getDocument();

    /**
     * Generates name for output as row number in case
     * <tt>{@link #isOutputRowNumbers()} == true</tt>
     */
    protected String getRowName(XSSFRow row) {
        return String.valueOf(row.getRowNum() + 1);
    }

    protected boolean isTextEmpty(XSSFCell cell) {
        final String value;
        switch (cell.getCellTypeEnum()) {
            case STRING:
                // XXX: enrich
                value = cell.getRichStringCellValue().getString();
                break;
            case FORMULA:
                switch (cell.getCachedFormulaResultTypeEnum()) {
                    case STRING:
                        XSSFRichTextString str = cell.getRichStringCellValue();
                        if (str.length() <= 0) {
                            return false;
                        }

                        value = str.toString();
                        break;
                    case NUMERIC:
                        XSSFCellStyle style = cell.getCellStyle();
                        double nval = cell.getNumericCellValue();
                        short df = style.getDataFormat();
                        String dfs = style.getDataFormatString();
                        value = formatter.formatRawCellContents(nval, df, dfs);
                        break;
                    case BOOLEAN:
                        value = String.valueOf(cell.getBooleanCellValue());
                        break;
                    case ERROR:
                        value = ErrorEval.getText(cell.getErrorCellValue());
                        break;
                    default:
                        value = XlsxToHtmlUtils.EMPTY;
                        break;
                }
                break;
            case BLANK:
                value = XlsxToHtmlUtils.EMPTY;
                break;
            case NUMERIC:
                value = formatter.formatCellValue(cell);
                break;
            case BOOLEAN:
                value = String.valueOf(cell.getBooleanCellValue());
                break;
            case ERROR:
                value = ErrorEval.getText(cell.getErrorCellValue());
                break;
            default:
                return true;
        }

        return XlsxToHtmlUtils.isEmpty(value);
    }

}
