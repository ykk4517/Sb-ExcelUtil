package com.ykkblog.excelutil.util;

import com.ykkblog.fastbase.BaseItem;
import com.ykkblog.fastbase.exception.BusinessException;
import com.ykkblog.fastbase.util.ConstantBase;
import com.ykkblog.fastbase.util.DataUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.util.ZipEntrySource;
import org.apache.poi.openxml4j.util.ZipFileZipEntrySource;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.util.TempFile;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import static com.ykkblog.fastbase.util.ConstantBase.*;

/**
 * Excel工具类
 *
 * @author 姚康康
 * @date 2021/9/24 0:50
 */
@Slf4j
public class ExcelUtil {

    /**
     * 读取Excel文件
     *
     * @param is       文件流
     * @param fileName 文件名
     * @return Workbook对象
     */
    public static Workbook readFile(InputStream is, String fileName) {
        try {
            String[] pfix = fileName.split(DOT);
            String suffix = pfix[pfix.length - 1];
            if (XLS.equals(suffix)) {
                log.info("本次读取的文件格式为：" + XLS);
                // jxl方法可读取.xls格式
                // 构造Workbook（工作薄）对象
                Workbook wbWorkbook;
                wbWorkbook = WorkbookFactory.create(is);
                return wbWorkbook;
            } else if (XLSX.equals(suffix)) {
                log.info("本次读取的文件格式为：" + XLSX);
                // poi方法可读取Excel2007即.xlsx格式
                XSSFWorkbook xssfWorkbook;
                xssfWorkbook = new XSSFWorkbook(is);
                return xssfWorkbook;
            } else {
                throw new BusinessException("文件格式无法识别，请检查文件格式或联系管理员！");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            log.error("初始化Workbook时出错" + e);
            throw new BusinessException("初始化Workbook时出错请检查文件格式或联系管理员！");
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    /**
     * 调整单元格样式基础配置
     *
     * @param workbook workbook
     * @param textSize 文字大小
     * @param bold     是否加粗
     * @return HSSFCellStyle
     */
    public static CellStyle getCellStyle(Workbook workbook, int textSize, boolean bold, Boolean autoWrap, ExcelConstant.ExcelColor excelColor) {
        CellStyle cellStyle = workbook.createCellStyle();
        // 水平居中
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        // 垂直居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        // 设置字体
        Font font = workbook.createFont();
        // 设置字体
        font.setFontName("宋体");
        // 设置标题字体大小
        font.setFontHeightInPoints((short) textSize);
        // 加粗
        font.setBold(bold);
        // 颜色处理
        if (excelColor != null) {
            setColor(cellStyle, font, excelColor);
        }
        // 设置字体样式
        cellStyle.setFont(font);
        // 设置边框
        setBorderStyle(BorderStyle.THIN, cellStyle);

        // 自动换行
        cellStyle.setWrapText(autoWrap);
        return cellStyle;
    }

    /**
     * 单元格设置颜色
     *
     * @param cellStyle  单元格样式
     * @param font       单元格字体
     * @param excelColor 字体颜色
     */
    public static void setColor(CellStyle cellStyle, Font font, ExcelConstant.ExcelColor excelColor) {
        // 背景颜色
        switch (excelColor) {
            case BLUE:
                // 设置背景色
                cellStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.LIGHT_CORNFLOWER_BLUE.getIndex());
                //XSSFColor xssfColor = new XSSFColor(new java.awt.Color(204, 204, 255));
                //XSSFColor xssfColor = new XSSFColor(new byte[] {(byte) 204, (byte) 204, (byte) 255}, new DefaultIndexedColorMap());
                //cellStyle.setFillForegroundColor(xssfColor.getIndexed());
                cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                break;
            case RED:
                // 黄色背景
                cellStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.LEMON_CHIFFON.getIndex());
                cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                // 设置字体颜色
                font.setColor(HSSFFont.COLOR_RED);
                break;
            default:
        }
    }


    /**
     * 单元格设置边框
     *
     * @param border 边框样式
     * @param region 合并区域
     * @param sheet  sheet
     */
    public static void setBorderStyle(BorderStyle border, CellRangeAddress region, Sheet sheet) {
        // 下边框
        RegionUtil.setBorderBottom(border, region, sheet);
        // 左边框
        RegionUtil.setBorderLeft(border, region, sheet);
        // 右边框
        RegionUtil.setBorderRight(border, region, sheet);
        // 上边框
        RegionUtil.setBorderTop(border, region, sheet);
    }

    /**
     * 单元格设置边框
     *
     * @param border 边框样式
     * @param sheet  sheet
     */
    public static CellStyle getBorderStyle(BorderStyle border, Sheet sheet) {
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();

        // 左边框 虚线 DASHED 破折号
        cellStyle.setBorderLeft(border);
        // 上边框 虚线 DOTTED 点虚线
        cellStyle.setBorderTop(border);
        // 右边框 加粗 THICK
        cellStyle.setBorderRight(border);
        // 下边框 正常 THIN
        cellStyle.setBorderBottom(border);
        return cellStyle;
    }

    /**
     * 单元格设置边框
     *
     * @param border    边框样式
     * @param cellStyle 单元格样式
     */
    public static void setBorderStyle(BorderStyle border, CellStyle cellStyle) {
        // 左边框 虚线 DASHED 破折号
        cellStyle.setBorderLeft(border);
        // 上边框 虚线 DOTTED 点虚线
        cellStyle.setBorderTop(border);
        // 右边框 加粗 THICK
        cellStyle.setBorderRight(border);
        // 下边框 正常 THIN
        cellStyle.setBorderBottom(border);
        // cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
    }

    /**
     * 设置列宽度
     * https://www.jianshu.com/p/76c160ad1069
     * https://blog.csdn.net/qwkxq/article/details/53508736 https://blog.csdn.net/weixin_41167961/article/details/81737481
     *
     * @param value       value
     * @param colIndex    colIndex
     * @param columnWidth 列宽度集合
     */
    public static void getColumnWidth(String value, int colIndex, List<Integer> columnWidth) {
        // 调整宽度
        // 计算中文个数
        int count = 0, length = 0;
        for (char c : value.toCharArray()) {
            // 中文文字
            if (c >= 0x4E00 && c <= 0x9FA5) {
                count++;
                length += 256 * 3;
            }
            // 中文符号
            Character.UnicodeScript sc = Character.UnicodeScript.of(c);
            if (sc == Character.UnicodeScript.HAN) {
                count++;
                length += 256 * 2;
            }
        }

        // 计算所需宽度
        length += (value.length() - count) * 256;

        if (columnWidth.get(colIndex) < length) {
            columnWidth.set(colIndex, Math.min(length, 255 * 256));
        }
    }

    /**
     * 计算中文字符个数
     *
     * @param str 字符串
     * @return 中文字符个数
     */
    public static int calculateChineseNumber(String str) {
        int count = 0;
        String regEx = "[\\u4e00-\\u9fa5]";
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            for (int i = 0; i <= matcher.groupCount(); i++) {
                count = count + 1;
            }
        }
        return count;
    }

    /**
     * 用于将Excel表格中列号字母转成列索引，从0对应A开始
     * CellReference.convertColStringToIndex
     *
     * @param column 列号
     * @return 列索引
     */
    public static int columnToIndex(String column) {
        String regex = "[A-Z]+";
        if (!column.matches(regex)) {
            try {
                throw new Exception("Invalid parameter");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        int index = 0;
        char[] chars = column.toUpperCase().toCharArray();
        for (int i = 0; i < chars.length; i++) {
            index += ((int) chars[i] - (int) 'A' + 1)
                    * (int) Math.pow(26, chars.length - i - 1);
        }
        return index - 1;

    }

    /**
     * 用于将excel表格中列索引转成列号字母，从A对应0开始
     * CellReference.CellReference.convertNumToColString
     *
     * @param index 列索引
     * @return 列号
     */
    public static String indexToColumn(int index) {
        if (index < 0) {
            try {
                throw new BusinessException("Invalid parameter");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        StringBuilder column = new StringBuilder();
        do {
            if (column.length() > 0) {
                index--;
            }

            column.insert(0, ((char) (index % 26 + (int) 'A')));

            index = (index - index % 26) / 26;

        } while (index > 0);

        return column.toString();

    }

    /**
     * 基于Response返回Excel
     *
     * @param response 返回流
     * @param workbook workbook
     * @param fileName 文件名
     */
    public static void exportByResponse(HttpServletResponse response, Workbook workbook, String fileName) {
        try {
            //response.setContentType("application/octet-stream");
            //response.setHeader("Content-Type", "application/octet-stream");
            // response.setContentType("application/vnd.ms-excel;charset=utf-8");
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            response.setHeader("Content-disposition", "attachment;filename="
                    + URLEncoder.encode(fileName, "utf-8") + (fileName.contains(XLS) ? "" : XLSX2));
            response.flushBuffer();
            OutputStream outputStream = response.getOutputStream();
            workbook.write(response.getOutputStream());
            workbook.close();
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("导出文件异常：" + e.getMessage());
            throw new BusinessException("很抱歉未能成功导出，请稍后重试或联系管理员！");
        }
    }

    /**
     * 基于File返回路径
     *
     * @param workbook workbook
     * @param path     存放路径
     */
    public static void exportByFile(Workbook workbook, String path) {
        try {
            FileOutputStream fileout = new FileOutputStream(path);
            // 执行公式
            // xssfWorkbook.setForceFormulaRecalculation(true);
            workbook.write(fileout);
            log.info("导出成功！");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("导出文件异常：" + e.getMessage());
            throw new BusinessException("很抱歉未能成功导出，请稍后重试或联系管理员！");
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException ignored) {
                }
            }
        }
    }

    /**
     * 基于workbook返回InputStream
     *
     * @param workbook workbook
     * @return InputStream InputStream
     */
    public static InputStream exportByInputStream(Workbook workbook) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            workbook.write(bos);
            return new ByteArrayInputStream(bos.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("导出文件流异常：" + e.getMessage());
            throw new BusinessException("很抱歉未能成功导出，请稍后重试或联系管理员！");
        }
    }

    /**
     * 基于workbook返回InputStream
     *
     * @param workbook workbook
     * @return InputStream InputStream
     */
    public static InputStream exportByInputStream2(Workbook workbook) {
        boolean deleted;
        File tmplFile = null;
        try {
            tmplFile = TempFile.createTempFile("poi-sxssf-template", ".xlsx");
            try (FileOutputStream os = new FileOutputStream(tmplFile)) {
                workbook.write(os);
            }

            //Substitute the template entries with the generated sheet data files
            return new FileInputStream(tmplFile);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("导出文件流异常：" + e.getMessage());
            throw new BusinessException("很抱歉未能成功导出，请稍后重试或联系管理员！");
        } finally {
            if (tmplFile != null) {
                deleted = tmplFile.delete();
            }
        }
    }

    /**
     * 判断有无Round函数， 保留了几位小数
     *
     * @param cellFormula Excel公式字符串
     * @param ws          小数位数
     * @return 处理后的公式 就是没有找到
     */
    public static String roundLength(String cellFormula, Integer ws) {

        String round = "round";
        if (DataUtil.isEmpty(cellFormula) || !cellFormula.toLowerCase().contains(round) || ws == null) {
            return cellFormula;
        }

        int front = 0;
        for (int i = cellFormula.toLowerCase().indexOf(round) + ConstantBase.BasicNumber.SIX; i < cellFormula.length(); i++) {
            String str = String.valueOf(cellFormula.charAt(i));
            if (")".equals(str)) {
                if (front == 0) {
                    // 找到闭合括号，截断至此时的i
                    String[] split = cellFormula.substring(0, i).split(",");
                    List<String> stringList = Arrays.asList(split);
                    String s = split[split.length - 1].replaceAll(REGEX_BLANK, "");
                    if (Integer.parseInt(s) != ws) {
                        stringList.set(split.length - 1, String.valueOf(ws));
                        return StringUtils.join(stringList.toArray(), ", ") + cellFormula.substring(i);
                    }
                    return cellFormula;
                } else {
                    front--;
                }
            } else if ("(".equals(str)) {
                front++;
            }
        }
        return cellFormula;
    }

    /**
     * 创建名称管理器
     *
     * @param wbWorkbook wbWorkbook
     * @param key        key
     * @param formula    formula
     */
    public static void createWorkName(Workbook wbWorkbook, String key, String formula) {
        // 添加名称管理器
        Name name = wbWorkbook.createName();
        // key不可重复,将父区域名作为key
        name.setNameName(key);
        name.setRefersToFormula(formula);
    }

    /**
     * 删除名称管理器
     *
     * @param wbWorkbook wbWorkbook
     * @param key        key
     */
    public static void deleteWorkName(Workbook wbWorkbook, String key) {
        // 删除名称管理器
        wbWorkbook.removeName(key);
    }

    /**
     * 获取范围坐标
     *
     * @param startRow    开始行（不是下标）
     * @param startCol    开始列（是下标，从0开始）
     * @param dataSize    数据长度
     * @param orientation 方向（0：水平；1：垂直）
     * @return "$A$12:$B$24"
     */
    public static String getRange(Integer startRow, Integer startCol, Integer dataSize, Integer orientation) {
        return getRange(startRow, startCol, dataSize, null, orientation);
    }

    /**
     * 获取范围坐标
     *
     * @param startRow    开始行（不是下标）
     * @param startCol    开始列（是下标，从0开始）
     * @param dataSize    数据长度
     * @param columnSize  列长度
     * @param orientation 方向（0：水平；1：垂直）
     * @return "$A$12:$B$24"
     */
    public static String getRange(Integer startRow, Integer startCol, Integer dataSize, Integer columnSize, Integer orientation) {

        if (orientation == 0) {
            // 水平方向
            return String.format("$%s$%d:$%s$%d", ExcelUtil.indexToColumn(startCol), startRow, ExcelUtil.indexToColumn(startCol + dataSize), startRow);
        } else if (orientation == 1) {
            // 垂直方向
            String startColumn = ExcelUtil.indexToColumn(startCol);
            return String.format("$%s$%d:$%s$%d", startColumn, startRow, startColumn, startRow + dataSize - 1);
        } else {
            return String.format("$%s$%d:$%s$%d", ExcelUtil.indexToColumn(startCol), startRow, ExcelUtil.indexToColumn(startCol + columnSize), startRow + dataSize);
        }
    }

    /**
     * 设置下拉框数据
     *
     * @param wb              表格对象
     * @param mainSheet       要渲染的sheet
     * @param hiddenSheetName 数据字典sheet名称
     * @param values          级联下拉数据
     * @param fatherCol       父级下拉区域
     */
    public static void setDropDownBox(Workbook wb, Sheet mainSheet, String hiddenSheetName, String keyName,
                                      List<BaseItem> values, Integer fatherCol) {
        // 获取所有sheet页个数
        int sheetTotal = wb.getNumberOfSheets();
        // 处理下拉数据
        if (values != null && values.size() != 0) {
            //新建一个sheet页
            Sheet hiddenSheet = wb.getSheet(hiddenSheetName);
            if (hiddenSheet == null) {
                hiddenSheet = wb.createSheet(hiddenSheetName);
                sheetTotal++;
            }
            // 获取数据起始行
            int endRowNum = hiddenSheet.getLastRowNum() + 1;
            Row fRow = hiddenSheet.createRow(endRowNum++);
            for (int i = 0; i < values.size(); i++) {
                fRow.createCell(i).setCellValue(values.get(i).getCode() + UNDERLINE + values.get(i).getName());
            }
            // 添加名称管理器
//            String formula = hiddenSheetName + "!" + getRange(endRowNum, 0, values.size() - 1, 0);
//            createWorkName(wb, keyName, formula);

            //将数据字典sheet页隐藏掉
            wb.setSheetHidden(sheetTotal - 1, true);

            // 设置父级下拉
            //获取新sheet页内容
            String mainFormula = hiddenSheetName + "!" + getRange(endRowNum, 0, values.size() - 1, 0);
            // 设置下拉列表值绑定到主sheet页具体哪个单元格起作用
            mainSheet.addValidationData(SetDataValidation(wb, mainFormula, 2, 65535, fatherCol, fatherCol));
        }
    }

    /**
     * 设置二级级联下拉框数据
     *
     * @param wb              表格对象
     * @param typeName        要渲染的sheet名称
     * @param hiddenSheetName 数据字典sheet名称
     * @param values          级联下拉数据
     * @param fatherCol       父级下拉区域
     * @param sonCol          子级下拉区域
     */
    public static void setSeconCascadeDropDownBox(XSSFWorkbook wb, String typeName, String hiddenSheetName,
                                                  Map<String, String[]> values, Integer fatherCol, Integer sonCol) {
        // 获取所有sheet页个数
        int sheetTotal = wb.getNumberOfSheets();
        // 处理下拉数据
        if (values != null && values.size() != 0) {
            //新建一个sheet页
            XSSFSheet hiddenSheet = wb.getSheet(hiddenSheetName);
            if (hiddenSheet == null) {
                hiddenSheet = wb.createSheet(hiddenSheetName);
                sheetTotal++;
            }
            // 获取数据起始行
            int startRowNum = hiddenSheet.getLastRowNum() + 1;
            int endRowNum = startRowNum;
            Set<String> keySet = values.keySet();
            for (String key : keySet) {
                XSSFRow fRow = hiddenSheet.createRow(endRowNum++);
                fRow.createCell(0).setCellValue(key);
                String[] sons = values.get(key);
                for (int i = 1; i <= sons.length; i++) {
                    fRow.createCell(i).setCellValue(sons[i - 1]);
                }
                // 添加名称管理器
                String range = getRange(endRowNum, 1, sons.length, 0);
                Name name = wb.createName();
                //key不可重复
                name.setNameName(key);
                String formula = hiddenSheetName + "!" + range;
                name.setRefersToFormula(formula);
            }
            //将数据字典sheet页隐藏掉
            wb.setSheetHidden(sheetTotal - 1, true);

            // 设置父级下拉
            //获取新sheet页内容
            String mainFormula = hiddenSheetName + "!$A$" + ++startRowNum + ":$A$" + endRowNum;
            XSSFSheet mainSheet = wb.getSheet(typeName);
            // 设置下拉列表值绑定到主sheet页具体哪个单元格起作用
            mainSheet.addValidationData(SetDataValidation(wb, mainFormula, 1, 65535, fatherCol, fatherCol));

            // 设置子级下拉
            // 当前列为子级下拉框的内容受父级哪一列的影响
            String indirectFormula = "INDIRECT($" + decimalToTwentyHex(fatherCol + 1) + "2)"; //=INDIRECT($C2)
            mainSheet.addValidationData(SetDataValidation(wb, indirectFormula, 1, 65535, sonCol, sonCol));
        }
    }

    /**
     * 返回类型 DataValidation
     *
     * @param wb         表格对象
     * @param strFormula formula
     * @param firstRow   起始行
     * @param endRow     终止行
     * @param firstCol   起始列
     * @param endCol     终止列
     * @return 返回类型 DataValidation
     */
    public static DataValidation SetDataValidation(Workbook wb, String strFormula, int firstRow, int endRow, int firstCol, int endCol) {
        CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);
        DataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) wb.getSheet("typelist"));
        DataValidationConstraint formulaListConstraint = dvHelper.createFormulaListConstraint(strFormula);
        DataValidation validation = dvHelper.createValidation(formulaListConstraint, regions);
        validation.setSuppressDropDownArrow(true);
        validation.setShowErrorBox(true);
        return validation;
    }

    /**
     * 十进制转二十六进制
     */
    private static String decimalToTwentyHex(int decimalNum) {
        StringBuilder result = new StringBuilder();
        while (decimalNum > 0) {
            int remainder = decimalNum % 26;
            result.append((char) (remainder + 64));//大写A的ASCII码值为65
            decimalNum = decimalNum / 26;
        }
        return result.reverse().toString();
    }

    /**
     * 设置文档作者
     *
     * @param workbook workbook
     * @param author   author
     */
    public static void setAuthor(Workbook workbook, String author) {
        if (workbook == null) {
            return;
        }
        if (author == null) {
            author = "";
        }
        try {
            if (workbook instanceof SXSSFWorkbook) {
                ((SXSSFWorkbook) workbook).getXSSFWorkbook().getProperties().getCoreProperties().setCreator(author);
            } else if (workbook instanceof XSSFWorkbook) {
                ((XSSFWorkbook) workbook).getProperties().getCoreProperties().setCreator(author);
            } else if (workbook instanceof HSSFWorkbook) {
                ((HSSFWorkbook) workbook).getSummaryInformation().setAuthor(author);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("设置文档信息失败！");
        }
    }

    /**
     * 设置文档信息
     *
     * @param workbook    workbook
     * @param application application
     */
    public static void setApplication(Workbook workbook, String application) {
        if (workbook == null) {
            return;
        }
        if (application == null) {
            application = "";
        }
        try {
            if (workbook instanceof SXSSFWorkbook) {
                ((SXSSFWorkbook) workbook).getXSSFWorkbook().getProperties().getExtendedProperties().getUnderlyingProperties().setApplication(application);
            } else if (workbook instanceof XSSFWorkbook) {
                ((XSSFWorkbook) workbook).getProperties().getExtendedProperties().getUnderlyingProperties().setApplication(application);
            } else if (workbook instanceof HSSFWorkbook) {
                ((HSSFWorkbook) workbook).getSummaryInformation().setApplicationName(application);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("设置文档信息失败！");
        }
    }

    /**
     * 设置单元格类型为文本
     *
     * @param cellStyle cellStyle
     */
    public static void getCellTypeStr(Workbook workbook, CellStyle cellStyle) {
        cellStyle.setDataFormat(workbook.createDataFormat().getFormat("@"));
    }

    /**
     * 设置单元格类型为文本
     *
     * @param cell cell
     * @return CellStyle
     */
    public static CellStyle getCellTypeStr(Cell cell) {
        CellStyle cellStyle = cell.getSheet().getWorkbook().createCellStyle();
        cellStyle.setDataFormat(cell.getSheet().getWorkbook().createDataFormat().getFormat("@"));
        return cellStyle;
    }

    /**
     * 设置单元格类型为文本
     *
     * @param workbook workbook
     * @return CellStyle
     */
    public static CellStyle getCellTypeStr(Workbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setDataFormat(workbook.createDataFormat().getFormat("@"));
        return cellStyle;
    }

    public static void main(String[] args) throws FileNotFoundException {
        // String path = "C:\\Users\\YKK\\Desktop\\人事档案导入模板.xlsx";
        /*String path = "C:\\Users\\YKK\\Desktop\\slary\\人力资源管理系统-基础信息采集表.xls";
        InputStream inputStream = new FileInputStream(path);*/

        System.out.println(columnToIndex("aL"));
        System.out.println(indexToColumn(0));
        System.out.println(indexToColumn(1));
        System.out.println(indexToColumn(18));


    }

}
