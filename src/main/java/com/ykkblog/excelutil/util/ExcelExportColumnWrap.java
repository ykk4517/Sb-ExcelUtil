package com.ykkblog.excelutil.util;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Excel换行列
 *
 * @author 姚康康
 * @date 2021/9/9 20:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ExcelExportColumnWrap extends ExcelExportColumn {

    public ExcelExportColumnWrap() {
        super.setAutoWrap(true);
    }

    /**
     * 换行列
     *
     * @param name 列名
     */
    public ExcelExportColumnWrap(String name) {
        this();
        super.setName(name);
    }

    /**
     * 换行列
     *
     * @param name 列名
     * @param cal  是否合计
     */
    public ExcelExportColumnWrap(String name, boolean cal) {
        this(name);
        super.setCal(cal);
    }

    /**
     * 换行列
     *
     * @param name        列名
     * @param columnWidth 列宽度
     */
    public ExcelExportColumnWrap(String name, Integer columnWidth) {
        this(name);
        super.setColumnWidth(columnWidth);
    }

    /**
     * 换行列
     *
     * @param name        列名
     * @param key        列key
     * @param columnWidth 列宽度
     */
    public ExcelExportColumnWrap(String name, String key, Integer columnWidth) {
        this(name, columnWidth);
        super.setKey(key);
    }

    /**
     * 换行列
     *
     * @param name        列名
     * @param cal         是否合计
     * @param columnWidth 列宽度
     */
    public ExcelExportColumnWrap(String name, boolean cal, Integer columnWidth) {
        this(name, cal);
        super.setColumnWidth(columnWidth);
    }

    /**
     * 换行列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     */
    public ExcelExportColumnWrap(String name, Integer columnSize, Integer contentSize) {
        super(name, columnSize, contentSize, false, true);
    }

    /**
     * 换行列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     * @param cal         是否合计
     */
    public ExcelExportColumnWrap(String name, Integer columnSize, Integer contentSize, boolean cal) {
        this(name, columnSize, contentSize);
        super.setCal(cal);
    }

    /**
     * 换行列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     * @param columnWidth 列宽度
     */
    public ExcelExportColumnWrap(String name, Integer columnSize, Integer contentSize, Integer columnWidth) {
        super(name, columnSize, contentSize, columnWidth);
    }

    /**
     * 换行列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     * @param cal         是否合计
     * @param columnWidth 列宽度
     */
    public ExcelExportColumnWrap(String name, Integer columnSize, Integer contentSize, boolean cal, Integer columnWidth) {
        this(name, columnSize, contentSize, cal);
        super.setColumnWidth(columnWidth);
    }

}
