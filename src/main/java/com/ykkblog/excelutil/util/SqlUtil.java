package com.ykkblog.excelutil.util;

import com.ykkblog.fastbase.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.jexl3.*;
import org.springframework.jdbc.core.JdbcTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Sql工具类
 *
 * @author YKK
 * @date 2021/9/10 18:20
 **/
@Slf4j
public class SqlUtil {

    /**
     * 执行Sql
     *
     * @param jdbcTemplate jdbcTemplate
     * @param sql          sql
     * @return List<Map < String, Object>>
     */
    public static List<Map<String, Object>> queryForList(JdbcTemplate jdbcTemplate, String sql) {
        try {
            return jdbcTemplate.queryForList(sql);
        } catch (Exception e) {
            log.error("Sql语法错误：" + e.getMessage());
            e.printStackTrace();
            throw new BusinessException("Sql语法错误！");
        }
    }

    /**
     * 执行Sql
     *
     * @param jdbcTemplate jdbcTemplate
     * @param sql          sql
     * @return List<Map < String, Object>>
     */
    public static Map<String, Object> queryForMap(JdbcTemplate jdbcTemplate, String sql) {
        try {
            return jdbcTemplate.queryForMap(sql);
        } catch (Exception e) {
            log.error("Sql语法错误：" + e.getMessage());
            e.printStackTrace();
            throw new BusinessException("Sql语法错误！");
        }
    }

    /**
     * 拼接sql
     *
     * @param sql    原始sql
     * @param params 入参参数
     * @param debug  是否打印日志
     * @return 拼接后的sql
     * @throws BusinessException 业务异常
     */
    public static String generateSql(String sql, Map<String, Object> params, boolean debug) throws BusinessException {
        long startTimeStamp = System.currentTimeMillis();
        // 选择语句处理
        sql = chooseSql(sql, params, debug);
        // 去掉换行符
        final String removeEnterRegex = "[\r\n]";
        // 找到所有参数
        final String regexParam = "\\$([A-Za-z0-9_]*)";
        // 判断是否是in函数
        final String inRegex = "(?i)(?:in)\\s*\\(\\s*(\\$[A-Za-z0-9_]*)";
        // 查找标签
        final String regexReplace = "\\{\\s*R\\s+([\\s\\S]*?)}([\\s\\S]*?)\\{/R}";
        // String regexBlank = "[\\s*|\\t\\r\\n　{}]";
        // 忽略标签大小写
        Pattern pattern = Pattern.compile(regexReplace, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(sql);
        JexlEngine jexl = new JexlBuilder().create();
        JexlContext jc = new MapContext();
        JexlExpression expression;
        while (matcher.find()) {
            String originalSqlPart = matcher.group();
            if (debug) {
                log.info("原始标签：" + originalSqlPart);
            }
            String expStr = matcher.group(1).replaceAll(removeEnterRegex, "");
            if (debug) {
                log.info("表达式：" + expStr);
            }
            // 找到表达式所有参数
            Pattern pattern2 = Pattern.compile(regexParam);
            Matcher matcher2 = pattern2.matcher(expStr);
            while (matcher2.find()) {
                jc.set(matcher2.group(), params.getOrDefault(matcher2.group(1), null));
            }
            try {
                expression = jexl.createExpression(expStr);
                Object evaluate = expression.evaluate(jc);
                if ((Boolean) evaluate) {
                    String sqlBody = matcher.group(2).replaceAll(removeEnterRegex, "");
                    if (debug) {
                        log.info("方法体：" + sqlBody);
                    }
                    pattern2 = Pattern.compile(inRegex);
                    matcher2 = pattern2.matcher(sqlBody);
                    // 如果是in函数
                    while (matcher2.find()) {
                        // in函数 in ($deptIds
                        if (debug) {
                            log.info("in函数：" + matcher2.group());
                            // $deptIds
                            log.info("jc变量名：" + matcher2.group(1));
                        }
                        String inStr = "'" + String.join("', '", String.valueOf(jc.get(matcher2.group(1))).split(",")) + "'";
                        sql = sql.replace(originalSqlPart, sqlBody.replace(matcher2.group(1), inStr));
                    }
                    sql = sql.replace(originalSqlPart, sqlBody);
                } else {
                    if (debug) {
                        log.info("删除标签：" + originalSqlPart);
                    }
                    sql = sql.replace(originalSqlPart, "");
                }
            } catch (Exception e) {
                // 报错日志
                throw new BusinessException("【" + expStr + "】" + e.getMessage().split("@1")[1]);
            }
        }
        // 找到表达式所有参数
        pattern = Pattern.compile(regexParam);
        matcher = pattern.matcher(sql);
        while (matcher.find()) {
            Object orDefault = params.getOrDefault(matcher.group().substring(1), "");
            // 防止sql注入
            sql = sql.replace(matcher.group(), orDefault == null ? "" : String.valueOf(orDefault).replaceAll("[\\\\']", ""));
        }
        sql = sql.replaceAll("\r\n\r\n", "\r\n").replaceAll("\r\n \r\n", "\r\n")
                .replaceAll(" \r\n\r\n", "\r\n").replaceAll(" \r\n \r\n", "\r\n");
        if (debug) {
            log.info("=================================<sql=================================\r\n" + sql);
            log.info("=================================sql>=================================");
        }
        log.info("生成Sql耗时：" + (System.currentTimeMillis() - startTimeStamp) + "ms");
        return sql;
    }

    /**
     * if判断语句
     *
     * @param sql    原始sql
     * @param params 参数
     * @param debug  是否打印日志
     * @return 返回sql
     */
    public static String chooseSql(String sql, Map<String, Object> params, boolean debug) {
        // 去掉换行符
        final String removeEnterRegex = "[\r\n]";
        // 找到所有参数
        final String regexParam = "\\$([A-Za-z0-9_]*)";
        // 查找标签
        final String regexReplace = "\\{\\s*IF\\s+([\\s\\S]*?)}([\\s\\S]*?)\\{/IF}";
        Pattern pattern = Pattern.compile(regexReplace, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(sql);
        JexlEngine jexl = new JexlBuilder().create();
        JexlContext jc = new MapContext();
        JexlExpression expression;
        while (matcher.find()) {
            String originalSqlPart = matcher.group();
            if (debug) {
                log.info("原始标签：" + originalSqlPart);
            }
            String expStr = matcher.group(1).replaceAll(removeEnterRegex, "");
            if (debug) {
                log.info("表达式：" + expStr);
            }
            // 找到表达式所有参数
            Pattern pattern2 = Pattern.compile(regexParam);
            Matcher matcher2 = pattern2.matcher(expStr);
            while (matcher2.find()) {
                jc.set(matcher2.group(), params.getOrDefault(matcher2.group(1), null));
            }
            try {
                expression = jexl.createExpression(expStr);
                Object evaluate = expression.evaluate(jc);
                if ((Boolean) evaluate) {
                    // .replaceAll(removeEnterRegex, "");
                    String sqlBody = matcher.group(2);
                    if (debug) {
                        log.info("方法体：" + sqlBody);
                    }
                    sql = sql.replace(originalSqlPart, sqlBody);
                } else {
                    if (debug) {
                        log.info("删除标签：" + originalSqlPart);
                    }
                    sql = sql.replace(originalSqlPart, "");
                }
            } catch (Exception e) {
                // 报错日志
                throw new BusinessException("【" + expStr + "】" + (e.getMessage().contains("@1") ? e.getMessage().split("@1")[1] : e.getMessage()));
            }
        }
        return sql;
    }

    /**
     * 判断map是否有值且不为空
     *
     * @param params map
     * @param key    key
     * @return 是否存在
     */
    private static boolean hasParam(Map<String, Object> params, String key) {
        return params.containsKey(key) && params.get(key) != null && !"".equals(params.get(key));
    }


    public static List<String> findIf(String jexlSql) {
        List<String> ifList = new ArrayList<>();
        boolean finding = false;
        int startIndex = 0, endIndex = 0;
        int children = 0;
        String start = "@if", colse = "}";
        while (true) {
            String str = jexlSql.substring(Math.max(startIndex, endIndex));
            int currentIndex = str.indexOf(finding ? colse : start);
            if (currentIndex == -1) {
                break;
            }
            // currentIndex = currentIndex + (finding ? 1 : 3);
            if (finding) {
                str = str.substring(0, currentIndex);
                children = children + matches(str, start).size();
                if (children > 0) {
                    endIndex = endIndex + currentIndex + 1;
                    children--;
                    continue;
                } else {
                    endIndex = endIndex + currentIndex;
                }
                ifList.add(jexlSql.substring(startIndex - 3, endIndex) + "if" + jexlSql.charAt(endIndex));
                startIndex = endIndex + 1;
            } else {
                startIndex = startIndex + currentIndex + 3;
                endIndex = startIndex;
            }
            finding = !finding;
        }

        return ifList;
    }

    public static String parseJexl(String jexlSql, Map<String, Object> params) {
        JexlEngine jexl = new JexlBuilder().create();
        JexlContext jc = new MapContext();
        JexlExpression expression = null;
        return parseJexl(jexlSql, params, jexl, jc, expression, 0);
    }

    public static String parseJexl(String jexlSql, Map<String, Object> params, JexlEngine jexl, JexlContext jc, JexlExpression expression, int level) {
        //判断是否包含@if
        // List<String> results = matches(jexlSql, "@if([\\s\\S]*?)}");
        List<String> ifList = findIf(jexlSql);
        if (ifList.isEmpty()) {
            return jexlSql;
        }
        Boolean needAppendSql;
        if (params != null) {
            for (String key : params.keySet()) {
                jc.set(key, String.valueOf(params.getOrDefault(key, null)).replace("\\.", ""));
            }
        }
        for (String ifStr : ifList) {
            List<String> sqlFragment = matches(ifStr, "\\(([\\s\\S]*?)\\)|\\{([\\s\\S]*?)if\\}");
            String sqlExp = sqlFragment.get(0).trim().substring(1, sqlFragment.get(0).length() - 1);
            sqlExp = sqlExp.replace(":", "");
            try {
                expression = jexl.createExpression(sqlExp);
                needAppendSql = (Boolean) expression.evaluate(jc);
            } catch (Exception e) {
                // 报错日志
                throw new BusinessException("【" + sqlExp + "】" + e.getMessage().split(":")[1]);
            }
            if (needAppendSql) {
                jexlSql = jexlSql.replace(ifStr.replace("if}", "}"),
                        parseJexl(sqlFragment.get(1).trim().substring(1, sqlFragment.get(1).length() - 3), params, jexl, jc, expression, level + 1));
            } else {
                jexlSql = jexlSql.replace(ifStr.replace("if}", "}"), "");
            }
        }
        if (level == 0) {
            List<String> sqlFragmentParam = matches(jexlSql, "\\?\\d+(\\.[A-Za-z]+)?|:[A-Za-z0-9]+(\\.[A-Za-z]+)?");
            for (String key : sqlFragmentParam) {
                jexlSql = jexlSql.replace(key, String.valueOf(params.get(key.substring(1))));
            }
        }
        return jexlSql;
    }

    public static List<String> matches(String source, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(source);
        List<String> list = new ArrayList<>();
        while (matcher.find()) {
            list.add(matcher.group());
        }
        return list;
    }


    public static void testOgnl() {
        // 原始sql
        String sql1 = "select id, invite_code, phone, name \n"
                + "from user \n"
                + "where status = 1 \n"
                + "@if(:inviteCode != null and :inviteCode == '188') {"
                + " and (invite_code like '%:inviteCode%' OR OTHER = like '%:inviteCode%')"
                + "}";

        String sql = "";

        System.out.println("入参为：null");
        Map<String, Object> params0 = new HashMap<>(1);
        params0.put("inviteCode", null);
        sql = parseJexl(sql1, params0);
        System.out.println("结果为：" + sql);
        System.out.println("==================================");

        Map<String, Object> params = new HashMap<>(1);
        params.put("inviteCode", 1);

        System.out.println("入参为：" + params);
        sql = parseJexl(sql1, params);
        System.out.println("结果为：" + sql);
        System.out.println("==================================");

        Map<String, Object> params1 = new HashMap<>(1);
        params1.put("inviteCode", "188");

        System.out.println("入参为：" + params1);
        sql = parseJexl(sql1, params1);
        System.out.println("结果为：" + sql);
    }


    public static String sqlReplace(String sql, Map<String, Object> params) {
// 查找标签
        final String regexParam = "\\{\\s+T\\s+([^{/T}]*)}";
        // String regexBlank = "[\\s*|\\t\\r\\n　{}]";
        Pattern pattern = Pattern.compile(regexParam, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(sql);
        while (matcher.find()) {
            String group = matcher.group();
            System.out.println(group);
        }
        return "";
    }


    public static void main(String[] args) {

        // testOgnl();

        String sql = "SELECT\n" +
                "pp.*\n" +
                "\n" +
                "FROM\n" +
                "P_PERSON pp\n" +
                "LEFT JOIN SYS_DEPT sd ON pp.DEPT_ID = sd.ID\n" +
                "LEFT JOIN S_JOB_TYPE sjt ON pp.JOB_TYPE = sjt.ID\n" +
                "WHERE\n" +
                "\tsd.STATUS = 1 AND sd.IS_UNIFIED = 1\n" +
                "{R  $name != null and $name == '1'} and pp.name {/R} \n" +
                "{ REPLACE , and pp.card_no, cardNo } \n" +
                "and ( 1=1\n" +
                "{ REPLACE, and sd.code, parentCode, = } \n" +
                "{ replace, or sd.parent_code, parentCode } \n" +
                ")\n" +
                "\n" +
                "{ COMPARE, and pp.status, status } \n" +
                "{ RePLACE, and pp.ff_status, ffStatus } \n" +
                "{ COMPARE, and pp.job_type, jobType } \n" +
                "{ COMPARE, and pp.promotion_level, promotionLevel} \n" +
                "{ like, and pp.education, education }\n" +
                "\n" +
                "ORDER BY sd.ID, pp.UPDATE_TIME DESC, pp.ID, pp.CREATE_TIME";

        Map<String, Object> params = new HashMap<>(16);
        params.put("status", 1);
        params.put("parentCode", 128);

        String result = sqlReplace(sql, params);
        //System.out.println(result);

        //   固定标签       表达式                     sql语句
        // { replace, $parentCode != null and $parentCode ！= '', and (sd.code = '$parentCode' or sd.parent_code = $parentCode)}
        // { replace, $parentCode != null and $parentCode ！= '', and (sd.code like '%$parentCode%' or sd.parent_code = $parentCode)}
        // 1,2,358,698,7896
        // { replace, $parentCode != null and $parentCode ！= '', and sd.code in $parentCode_iN, name}
        // { replace, $parentCode != null and $parentCode ！= '', and sd.code in (select id, name from sys_dept where code = $parentCode), name}

        String paramValue = "698' or '1'='1";
        String temp = "select * from sys_user where status = 1 and sd.code =    " +
                " $parentCode";

        String key = "$parentCode";
        if (true) {
            temp = temp.replace(key, "'" + paramValue + "'");
        }
        System.out.println(temp);

        Map<String, BigDecimal> param = new HashMap<>(16);
        param.put("basic_salary", new BigDecimal("3953.9"));
        param.put("performance_salary", new BigDecimal("0.1"));
        param.put("aa", new BigDecimal("0.1"));

        JexlEngine jexl = new JexlBuilder().create();
        JexlContext jc = new MapContext();

        for (String s : param.keySet()) {
            jc.set(s, param.getOrDefault(s, BigDecimal.ZERO));
        }

        JexlExpression expression;

        // 重要！！！！

        String exp = "((basic_salary <= 3500 ? 3500 : basic_salary) + performance_salary)*2";
        try {
            expression = jexl.createExpression(exp);

            Object evaluate = expression.evaluate(jc);

            BigDecimal res = new BigDecimal(String.valueOf(evaluate));
            System.out.println(res);
        } catch (Exception e) {
            // 报错日志
            throw new BusinessException("【" + exp + "】" + e.getMessage().split("@1")[1]);
        }

    }


}
