package com.ykkblog.excelutil.util;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Excel合计列
 *
 * @author 姚康康
 * @date 2021/9/9 20:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ExcelExportColumnCal extends ExcelExportColumn {

    public ExcelExportColumnCal() {
        super.setCal(true);
    }

    /**
     * 合计列
     *
     * @param name 列名
     */
    public ExcelExportColumnCal(String name) {
        this();
        super.setName(name);
    }

    /**
     * 合计列
     *
     * @param name     列名
     * @param autoWrap 是否换行
     */
    public ExcelExportColumnCal(String name, boolean autoWrap) {
        this(name);
        super.setAutoWrap(autoWrap);
    }

    /**
     * 合计列
     *
     * @param name        列名
     * @param columnWidth 列宽度
     */
    public ExcelExportColumnCal(String name, Integer columnWidth) {
        this(name);
        super.setColumnWidth(columnWidth);
    }

    /**
     * 合计列
     *
     * @param name        列名
     * @param key         列key
     * @param columnWidth 列宽度
     */
    public ExcelExportColumnCal(String name, String key, Integer columnWidth) {
        this(name, columnWidth);
        super.setKey(key);
    }

    /**
     * 合计列
     *
     * @param name        列名
     * @param autoWrap    是否换行
     * @param columnWidth 列宽度
     */
    public ExcelExportColumnCal(String name, boolean autoWrap, Integer columnWidth) {
        this(name, autoWrap);
        super.setColumnWidth(columnWidth);
    }

    /**
     * 合计列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     */
    public ExcelExportColumnCal(String name, Integer columnSize, Integer contentSize) {
        super(name, columnSize, contentSize, true, false);
    }

    /**
     * 合计列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     * @param autoWrap    是否换行
     */
    public ExcelExportColumnCal(String name, Integer columnSize, Integer contentSize, boolean autoWrap) {
        this(name, columnSize, contentSize);
        super.setAutoWrap(autoWrap);
    }

    /**
     * 合计列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     * @param columnWidth 列宽度
     */
    public ExcelExportColumnCal(String name, Integer columnSize, Integer contentSize, Integer columnWidth) {
        this(name, columnSize, contentSize);
        super.setColumnWidth(columnWidth);
    }

    /**
     * 合计列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     * @param autoWrap    是否换行
     * @param columnWidth 列宽度
     */
    public ExcelExportColumnCal(String name, Integer columnSize, Integer contentSize, boolean autoWrap, Integer columnWidth) {
        this(name, columnSize, contentSize, autoWrap);
        super.setColumnWidth(columnWidth);
    }

}
