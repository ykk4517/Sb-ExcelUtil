package com.ykkblog.excelutil.util;

import org.apache.poi.xssf.usermodel.XSSFPictureData;

import java.util.Base64;

/**
 * @author YKK
 * @date 2022/3/4 15:36
 **/

public class PictureDataUtil {

    public static String pictureData2Base64(XSSFPictureData xssfPictureData) {
        byte[] data = xssfPictureData.getData();
        return Base64.getEncoder().encodeToString(data);
    }

}
