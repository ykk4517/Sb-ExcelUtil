package com.ykkblog.excelutil.util;

import com.ykkblog.excelutil.sxssf.SxssfExcelUtil;
import com.ykkblog.fastbase.exception.BusinessException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Excel配置类
 *
 * @author 姚康康
 * @date 2021/9/9 20:48
 */
@Slf4j
@Data
public class ExcelExportConfig {
    /**
     * 报表标题
     */
    private String title;
    /**
     * 报表描述
     */
    private String description;
    /**
     * sheet名称 | 文件名称前缀
     */
    private String fileNamePrefix;
    /**
     * 文件名称日期格式
     */
    private String fileNameFormat;
    /**
     * sql
     */
    private String sql;
    /**
     * 标题字体大小
     */
    @Min(1)
    private int titleSize = 12;
    /**
     * 列头字体大小
     */
    @Min(1)
    private int columnSize = 10;
    /**
     * 内容字体大小
     */
    @Min(1)
    private int contentSize = 10;
    /**
     * 是否显示序号
     */
    private boolean rowNum = true;

    /**
     * 冻结列数
     */
    private Integer freezeColumnNum = 2;

    /**
     * 导出是否出错
     */
    private boolean error = false;

    /**
     * 列配置
     */
    private List<ExcelExportColumn> columnList;

    /**
     * 列数据
     */
    private List<List<Object>> data;

    /**
     * 列数据
     */
    private List<Map<String, Object>> mapData;

    /**
     * 常规构造方法【list方式取数】
     *
     * @param title          报表标题
     * @param fileNamePrefix sheet名称 | 文件名称前缀
     * @param columnList     列配置
     * @param data           列数据
     */
    public ExcelExportConfig(String title, String fileNamePrefix, List<ExcelExportColumn> columnList, List<List<Object>> data) {
        this.title = title;
        this.fileNamePrefix = fileNamePrefix;
        this.columnList = columnList;
        this.data = data;
    }

    /**
     * 常规构造方法【map方式取数】
     *
     * @param columnList     列配置
     * @param mapData        列数据
     * @param fileNamePrefix sheet名称 | 文件名称前缀
     * @param title          报表标题
     */
    public ExcelExportConfig(List<ExcelExportColumn> columnList, List<Map<String, Object>> mapData, String fileNamePrefix, String title) {
        this.title = title;
        this.fileNamePrefix = fileNamePrefix;
        this.columnList = columnList;
        this.mapData = mapData;
    }

    public ExcelExportConfig setRowNum(boolean rowNum) {
        this.rowNum = rowNum;
        return this;
    }

    public ExcelExportConfig setFreezeColumnNum(Integer freezeColumnNum) {
        this.freezeColumnNum = freezeColumnNum;
        return this;
    }

    /**
     * 一键导出报表
     * @param response response
     */
    public void export(HttpServletResponse response) {
        export(response, null);
    }

    /**
     * 一键导出报表
     * @param response response
     * @param startTimeMillis startTimeMillis
     */
    public void export(HttpServletResponse response, Long startTimeMillis) {
        // 导出Excel
        log.info("===============================");
        long millis, second;
        if (startTimeMillis == null) {
            startTimeMillis = System.currentTimeMillis();
        } else {
            millis = System.currentTimeMillis() - startTimeMillis;
            second = millis / 1000;
            log.info(title + "-查询数据：" + second + "s/ " + millis + "ms");
        }

        SXSSFWorkbook wb = null;
        try {
            log.info(title + "-总行数：" + data.size());
            log.info(title + "-渲染数据开始");

            SxssfExcelUtil sxssfExcelUtil = new SxssfExcelUtil(this);
            // 开始之前对SXSSFWorkbook做一些配置
            wb = sxssfExcelUtil.getSxssfWorkbookByPageThread(null);
            millis = System.currentTimeMillis() - startTimeMillis;
            second = millis / 1000;
            log.info(title + "-总用时：" + second + "s/ " + millis + "ms");
            log.info("===============================");
            System.out.println("正在导出！");
            sxssfExcelUtil.exportByResponse(response);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("未能成功导出【" + title + "】，请稍后重试或联系管理员！");
        } finally {
            if (wb != null) {
                wb.dispose();
            }
        }
    }
}
