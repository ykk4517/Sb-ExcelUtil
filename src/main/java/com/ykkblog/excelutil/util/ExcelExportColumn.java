package com.ykkblog.excelutil.util;

import lombok.Data;

/**
 * Excel列
 *
 * @author 姚康康
 * @date 2021/9/9 20:11
 */
@Data
public class ExcelExportColumn {
    /**
     * 列名称
     */
    private String name;
    /**
     * 列key
     */
    private String key;
    /**
     * 列头大小
     */
    private Integer columnSize;
    /**
     * 内容字体大小
     */
    private Integer contentSize;
    /**
     * 是否合计
     */
    private Boolean cal;
    /**
     * 是否自动换行
     */
    private Boolean autoWrap;
    /**
     * 指定列宽
     */
    private Integer columnWidth;

    public ExcelExportColumn() {
        this.columnSize = 14;
        this.contentSize = 12;
        this.cal = false;
        this.autoWrap = false;
    }

    /**
     * Excel列
     *
     * @param name 列名
     */
    public ExcelExportColumn(String name) {
        this();
        this.name = name;
    }

    /**
     * Excel列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     */
    public ExcelExportColumn(String name, Integer columnSize, Integer contentSize) {
        this(name);
        this.columnSize = columnSize;
        this.contentSize = contentSize;
    }

    /**
     * Excel列
     *
     * @param name     列名
     * @param autoWrap 是否换行
     */
    public ExcelExportColumn(String name, boolean autoWrap) {
        this(name);
        this.autoWrap = autoWrap;
    }

    /**
     * Excel列
     *
     * @param name        列名
     * @param columnWidth 列宽度
     */
    public ExcelExportColumn(String name, Integer columnWidth) {
        this(name);
        this.columnWidth = columnWidth;
    }

    /**
     * Excel列
     *
     * @param name        列名
     * @param key        列key
     * @param columnWidth 列宽度
     */
    public ExcelExportColumn(String name, String key, Integer columnWidth) {
        this(name);
        this.key = key;
        this.columnWidth = columnWidth;
    }

    /**
     * Excel列
     *
     * @param name     列名
     * @param cal      是否合计
     * @param autoWrap 是否换行
     */
    public ExcelExportColumn(String name, boolean cal, boolean autoWrap) {
        this(name);
        this.cal = cal;
        this.autoWrap = autoWrap;
    }

    /**
     * Excel列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     * @param autoWrap    是否换行
     */
    public ExcelExportColumn(String name, Integer columnSize, Integer contentSize, boolean autoWrap) {
        this(name, columnSize, contentSize);
        this.autoWrap = autoWrap;
    }

    /**
     * Excel列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     * @param cal         是否合计
     * @param autoWrap    是否换行
     */
    public ExcelExportColumn(String name, Integer columnSize, Integer contentSize, boolean cal, boolean autoWrap) {
        this(name, columnSize, contentSize, autoWrap);
        this.cal = cal;
    }

    /**
     * Excel列
     *
     * @param name        列名
     * @param columnSize  列大小
     * @param contentSize 列内容大小
     * @param columnWidth 列宽度
     */
    public ExcelExportColumn(String name, Integer columnSize, Integer contentSize, Integer columnWidth) {
        this(name, columnSize, contentSize);
        this.columnWidth = columnWidth;
    }

}
