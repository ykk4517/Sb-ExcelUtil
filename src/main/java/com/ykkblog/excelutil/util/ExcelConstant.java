package com.ykkblog.excelutil.util;

/**
 * Excel常量类
 *
 * @author YKK
 * @date 2021/9/9 20:11
 **/

public class ExcelConstant {

    /**
     * 基础状态对照码
     */
    public enum ExcelColor {
        /**
         * 默认不做处理
         */
        DEFAULT,
        /**
         * 蓝色
         */
        BLUE,
        /**
         * 红色
         */
        RED
    }

}
