package com.ykkblog.excelutil;


import com.ykkblog.excelutil.util.ExcelExportColumn;
import com.ykkblog.excelutil.util.ExcelExportConfig;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 主方法
 *
 * @author 姚康康
 * @date 2021/9/9 20:37
 */
public class Main {

    public static void main(String[] args) throws Exception {
         test1();
        //test3();
    }

    public static void test1() throws Exception {
        // write your code here
        String title = "测试报表";
        String basePath = "C:\\Users\\YKK\\Desktop\\";
        String fileNamePrefix = "测试表格";
        String path = basePath + File.separator + fileNamePrefix + ".xls";
        List<ExcelExportColumn> columnList = new ArrayList<ExcelExportColumn>() {{
            add(new ExcelExportColumn("姓名", 14, 12, false));
            add(new ExcelExportColumn("身份证号码", 14, 12, false));
            add(new ExcelExportColumn("手机号码", 14, 12, false));
            add(new ExcelExportColumn("余额", 14, 12, true));
        }};
        List<List<Object>> data = new ArrayList<>();
        List<Object> l = Arrays.asList("姚康康", "412727199510124538", "15603628720", 36.25F);
        data.add(l);
        data.add(l);
        data.add(l);

        ExcelExportConfig excelExportConfig = new ExcelExportConfig(title, fileNamePrefix, columnList, data);
        excelExportConfig.setFileNameFormat("yyyyMMddHHmmss");

        ExcelExportUtil excelExportUtil = new ExcelExportUtil(excelExportConfig);
        XSSFWorkbook xssfWorkbook = excelExportUtil.setListToExcel();

        // 生成文件
        String fileName = excelExportUtil.exportExcelByFile(basePath);
        if (fileName != null) {
            System.out.println("导出成功：" + basePath + fileName);
        } else {
            System.out.println("导出失败！！！");
        }

    }

    public static void test2() throws Exception {
        // write your code here
        String title = "测试报表";
        String basePath = "C:\\Users\\YKK\\Desktop";
        String fileNamePrefix = "测试表格";
        String path = basePath + File.separator + fileNamePrefix + ".xls";
        List<ExcelExportColumn> columnList = new ArrayList<ExcelExportColumn>() {{
            add(new ExcelExportColumn("批次编号", 12, 10, false));
            add(new ExcelExportColumn("明细编号", 12, 10, false));
            add(new ExcelExportColumn("单位编码", 12, 10, false));
            add(new ExcelExportColumn("单位名称", 12, 10, false));
            add(new ExcelExportColumn("补贴项目编码", 12, 10, false));
            add(new ExcelExportColumn("补贴项目名称", 12, 10, false));
            add(new ExcelExportColumn("备注", 12, 10, false));
            add(new ExcelExportColumn("应发人数", 12, 10, true));
            add(new ExcelExportColumn("应发金额", 12, 10, true));
            add(new ExcelExportColumn("实发人数", 12, 10, true));
            add(new ExcelExportColumn("实发金额", 12, 10, true));
            add(new ExcelExportColumn("当前节点", 12, 10, false));
            add(new ExcelExportColumn("是否首发", 12, 10, false));
            add(new ExcelExportColumn("状态", 12, 10, false));
            add(new ExcelExportColumn("预警数量", 12, 10, false));
            add(new ExcelExportColumn("经办人", 12, 10, false));
            add(new ExcelExportColumn("创建时间", 12, 10, false));
        }};
        List<List<Object>> data = new ArrayList<>();
        // List l = Arrays.asList("姚康康", "412727199510124538", "15603628720", 36.25F);

        data.add(Arrays.asList("40", "2020101287", "506001", "天峨县林业局", "A400000", "森林生态效益补偿补助", "下老乡公益林2064人，共1112354.99", 1753, 965485.52, 0, 0.0, "发起流程", "否", 0, 395, "发起人-孙浩永", "2020-10-12 09:57:06"));
        data.add(Arrays.asList("39", "2020101086", "525001", "龙滩自然保护区管理中心", "A400000", "森林生态效益补偿补助", "第五批", 22, 20310.0, 0, 0.0, "指标挂接", "否", 1, 0, "发起流程-陈金仙", "2020-10-10 15:45:36"));
        data.add(Arrays.asList("38", "2020101085", "525001", "龙滩自然保护区管理中心", "A400000", "森林生态效益补偿补助", "第四批", 22, 29080.0, 0, 0.0, "指标挂接", "否", 1, 0, "发起流程-陈金仙", "2020-10-10 15:40:23"));
        data.add(Arrays.asList("25", "2020101084", "525001", "龙滩自然保护区管理中心", "A400000", "森林生态效益补偿补助", "第三批", 12, 5642.0, 0, 0.0, "发起流程", "是", 0, 22, "发起流程-陈金仙", "2020-10-10 15:33:58"));
        data.add(Arrays.asList("24", "2020101083", "525001", "龙滩自然保护区管理中心", "A400000", "森林生态效益补偿补助", "第二批", 26, 30632.0, 0, 0.0, "发起流程", "是", 0, 70, "发起流程-陈金仙", "2020-10-10 15:30:56"));
        data.add(Arrays.asList("18", "2020101082", "525001", "龙滩自然保护区管理中心", "A400000", "森林生态效益补偿补助", "第一次", 15, 16442.0, 0, 0.0, "发起流程", "是", 0, 30, "发起流程-陈金仙", "2020-10-10 15:27:02"));
        data.add(Arrays.asList("37", "2020100981", "506001", "天峨县林业局", "A380000", "完善退耕还林政策补助", "岜暮乡02年度退耕还林政策兑现1370人合计119213.6元", 1245, 107572.4, 0, 0.0, "发起流程", "否", 0, 250, "发起人-韦义谋", "2020-10-09 18:44:25"));
        data.add(Arrays.asList("36", "2020100980", "506001", "天峨县林业局", "A380000", "完善退耕还林政策补助", "岜暮乡02年度退耕还林政策兑现1390人合计119213.6元", 0, 0.0, 0, 0.0, "发起流程", "否", 4, 0, "发起人-韦义谋", "2020-10-09 18:39:35"));
        data.add(Arrays.asList("35", "2020100979", "506001", "天峨县林业局", "A380000", "完善退耕还林政策补助", "岜暮乡02年退耕还林政策兑现1371人 共119213.6", 1245, 107572.4, 0, 0.0, "发起流程", "否", 4, 250, "发起人-韦义谋", "2020-10-09 18:35:08"));
        data.add(Arrays.asList("34", "2020100978", "506001", "天峨县林业局", "A400000", "森林生态效益补偿补助", "三堡乡2020年度公益林72人共21511.88元", 63, 20031.88, 0, 0.0, "发起流程", "否", 0, 17, "发起人-孙浩永", "2020-10-09 17:57:19"));

        ExcelExportConfig excelExportConfig = new ExcelExportConfig(title, fileNamePrefix, columnList, data);

        ExcelExportUtil excelExportUtil = new ExcelExportUtil(excelExportConfig);
        excelExportUtil.setListToExcel();
    }


    public static void test3() throws Exception {


        // write your code here
        String title = "测试报表";
        String basePath = "C:\\Users\\YKK\\Desktop";
        String fileNamePrefix = "测试表格";
        String path = basePath + File.separator + fileNamePrefix + ".xls";
        List<ExcelExportColumn> columnList = new ArrayList<ExcelExportColumn>() {{
            add(new ExcelExportColumn("批次编号", 12, 10, false));
            add(new ExcelExportColumn("明细编号", 12, 10, false));
            add(new ExcelExportColumn("单位编码", 12, 10, false));
            add(new ExcelExportColumn("单位名称", 12, 10, false));
            add(new ExcelExportColumn("补贴项目编码", 12, 10, false));
            add(new ExcelExportColumn("补贴项目名称", 12, 10, false));
            add(new ExcelExportColumn("备注", 12, 10, false));
            add(new ExcelExportColumn("应发人数", 12, 10, true));
            add(new ExcelExportColumn("应发金额", 12, 10, true));
            add(new ExcelExportColumn("实发人数", 12, 10, true));
            add(new ExcelExportColumn("实发金额", 12, 10, true));
            add(new ExcelExportColumn("当前节点", 12, 10, false));
            add(new ExcelExportColumn("是否首发", 12, 10, false));
            add(new ExcelExportColumn("状态", 12, 10, false));
            add(new ExcelExportColumn("预警数量", 12, 10, false));
            add(new ExcelExportColumn("经办人", 12, 10, false));
            add(new ExcelExportColumn("创建时间", 12, 10, false));
        }};
        List<List<Object>> data = new ArrayList<>();
        // List l = Arrays.asList("姚康康", "412727199510124538", "15603628720", 36.25F);

        int size = 5000;
        for (int i = 0; i < size; i++) {

            data.add(Arrays.asList("40", "2020101287", "506001", "天峨县林业局", "A400000", "森林生态效益补偿补助", "下老乡公益林2064人，共1112354.99", 1753, new BigDecimal("965485.52"), 0, 0.0, "发起流程", "否", 0, 395, "发起人-孙浩永", "2020-10-12 09:57:06"));
        }
        /*data.add(Arrays.asList("39", "2020101086", "525001", "龙滩自然保护区管理中心", "A400000", "森林生态效益补偿补助", "第五批", 22, 20310.0, 0, 0.0, "指标挂接", "否", 1, 0, "发起流程-陈金仙", "2020-10-10 15:45:36"));
        data.add(Arrays.asList("38", "2020101085", "525001", "龙滩自然保护区管理中心", "A400000", "森林生态效益补偿补助", "第四批", 22, 29080.0, 0, 0.0, "指标挂接", "否", 1, 0, "发起流程-陈金仙", "2020-10-10 15:40:23"));
        data.add(Arrays.asList("25", "2020101084", "525001", "龙滩自然保护区管理中心", "A400000", "森林生态效益补偿补助", "第三批", 12, 5642.0, 0, 0.0, "发起流程", "是", 0, 22, "发起流程-陈金仙", "2020-10-10 15:33:58"));
        data.add(Arrays.asList("24", "2020101083", "525001", "龙滩自然保护区管理中心", "A400000", "森林生态效益补偿补助", "第二批", 26, 30632.0, 0, 0.0, "发起流程", "是", 0, 70, "发起流程-陈金仙", "2020-10-10 15:30:56"));
        data.add(Arrays.asList("18", "2020101082", "525001", "龙滩自然保护区管理中心", "A400000", "森林生态效益补偿补助", "第一次", 15, 16442.0, 0, 0.0, "发起流程", "是", 0, 30, "发起流程-陈金仙", "2020-10-10 15:27:02"));
        data.add(Arrays.asList("37", "2020100981", "506001", "天峨县林业局", "A380000", "完善退耕还林政策补助", "岜暮乡02年度退耕还林政策兑现1370人合计119213.6元", 1245, 107572.4, 0, 0.0, "发起流程", "否", 0, 250, "发起人-韦义谋", "2020-10-09 18:44:25"));
        data.add(Arrays.asList("36", "2020100980", "506001", "天峨县林业局", "A380000", "完善退耕还林政策补助", "岜暮乡02年度退耕还林政策兑现1390人合计119213.6元", 0, 0.0, 0, 0.0, "发起流程", "否", 4, 0, "发起人-韦义谋", "2020-10-09 18:39:35"));
        data.add(Arrays.asList("35", "2020100979", "506001", "天峨县林业局", "A380000", "完善退耕还林政策补助", "岜暮乡02年退耕还林政策兑现1371人 共119213.6", 1245, 107572.4, 0, 0.0, "发起流程", "否", 4, 250, "发起人-韦义谋", "2020-10-09 18:35:08"));
        data.add(Arrays.asList("34", "2020100978", "506001", "天峨县林业局", "A400000", "森林生态效益补偿补助", "三堡乡2020年度公益林72人共21511.88元", 63, 20031.88, 0, 0.0, "发起流程", "否", 0, 17, "发起人-孙浩永", "2020-10-09 17:57:19"));*/

        ExcelExportConfig excelExportConfig = new ExcelExportConfig(title, fileNamePrefix, columnList, data);

        ExcelExportUtil excelExportUtil = new ExcelExportUtil(excelExportConfig);
        excelExportUtil.setListToExcel();

    }


    /**
     *
     *
     * // 设置Http响应头告诉浏览器下载这个附件
     *             /*response.setHeader("Content-Disposition", "attachment;Filename=" + System.currentTimeMillis() + ".xls");
     *             OutputStream outputStream = response.getOutputStream();
     *             wb.write(outputStream);
     *             outputStream.close();
     *             return wb.getBytes();
     *
     */
}
